<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['jwt_key'] = API_KEY;

$config['jwt_algorithm'] = 'HS256';
