<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config['error_prefix'] = '<i class="fa fa-times"></i> ';
$config['error_suffix'] = '</br>';