<?php
defined('BASEPATH') OR exit('No direct script access allowed');


defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);


defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);


defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');


defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

defined('SESS_ADMIN')      		OR define('SESS_ADMIN', 'a.'); 
defined('SESS_PUB')      		OR define('SESS_PUB', 'p.'); 
defined('MINIFIER') 			OR define('MINIFIER', 'buffer/');
defined('CAPTCHAPATH') 			OR define('CAPTCHAPATH', './assets/captcha/');

defined('BASE_PANEL') 			OR define('BASE_PANEL', '');
defined('ENCFK') 				OR define('ENCFK', 'MyPr0jecT5');
defined('DEF_APP') 				OR define('DEF_APP', 'default');
defined('DEBUG_FITABLE') 		OR define('DEBUG_FITABLE', (ENVIRONMENT == 'development') ? true : false);

defined('TEMPLATE') 			OR define('TEMPLATE', 'gentelella');
defined('TEMPL_VER') 			OR define('TEMPL_VER', 'v1');
defined('UPLOAD_PATH') 			OR define('UPLOAD_PATH', './assets/');
defined('ICON_PATH') 			OR define('ICON_PATH', 'image/icon/');
defined('THUMBNAME') 			OR define('THUMBNAME', '_thumbs');
defined('DEFAULTIMG') 			OR define('DEFAULTIMG', 'assets/image/default.png');
defined('UPLOADIMG') 			OR define('UPLOADIMG', 'image/image/uploader.png');
defined('MALEAVA') 	 			OR define('MALEAVA', 'assets/image/male_ava.png');
defined('FEMALEAVA') 			OR define('FEMALEAVA', 'assets/image/female_avatar.png');
defined('GMAPKEY') 				OR define('GMAPKEY', 'AIzaSyD1moNAkIldseLoMTY6J-fX04eEFkWnt_8');
defined('API_KEY') 				OR define('API_KEY', 'dhiDH@AWIAWDWd&7wd7q8wqyddwjqkhq67612836178sdqydq=');

defined('PROJECTID') 			OR define('PROJECTID', 'default');
defined('IS_TESTING') 			OR define('IS_TESTING', ENVIRONMENT == 'development' ? true : false);
defined('GSTORAGE') 			OR define('GSTORAGE', getenv('CI_ENV') != false ? 'gs://'.ENVIRONMENT.'-apps/' : APPPATH);

