<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// $uri        = $_SERVER['REQUEST_URI'];
// $lookup     = preg_match('/'.str_replace('/','',BASE_PANEL).'/', $uri);

// $cache_path = $lookup == true ? 'admin' : 'public';
// $lang  		= $lookup == true ? 'english' : 'english';
$cache_path = 'public';
$lang = 'indonesia';

if(ENVIRONMENT == 'development'){
	$compress_output 		= FALSE;
	$global_xss_filtering 	= FALSE;
}
else{
	$compress_output 		= FALSE;
	$global_xss_filtering	= FALSE;
}

//Nambahin biar bisa HTTP_X_FORWARDED_PROTO

//$config['base_url']  = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") || (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on')) ?  "https" : "http";

$config['base_url'] = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on" ) ? "https" : "http" );
$config['base_url'] .= "://".$_SERVER['HTTP_HOST'];
$config['base_url'] .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
// $config['base_url'] = '';

$config['index_page'] = '';

$config['uri_protocol']	= "AUTO";

$config['url_suffix'] = '';

$config['language']	= $lang;

$config['charset'] = 'UTF-8';

$config['enable_hooks'] = FALSE;

$config['subclass_prefix'] = 'FI_';

$config['composer_autoload'] = 'vendor/autoload.php';;

$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-';

$config['allow_get_array'] = TRUE;
$config['enable_query_strings'] = FALSE;
$config['controller_trigger'] = 'c';
$config['function_trigger'] = 'm';
$config['directory_trigger'] = 'd';

$config['log_threshold'] = 1;

$config['log_path'] = GSTORAGE.'logs/';
// $config['log_path'] = 'logs';

$config['log_file_extension'] = 'txt';

$config['log_file_permissions'] = 0644;

$config['log_date_format'] = 'Y-m-d H:i:s';

$config['error_views_path'] = '';

$config['cache_path'] = GSTORAGE.'cache/'.$cache_path.'/files/';

$config['cache_query_string'] = FALSE;

$config['encryption_key'] = 'My@ProJ3cT'.DEF_APP;

$config['sess_driver'] = 'files';
$config['sess_cookie_name'] = DEF_APP.'_session';
// $config['sess_expiration'] = 7200;
$config['sess_expiration'] = 86400; // diset selama 1 hari


// Untuk mac os yang dipakai seperti ini
$config['sess_save_path'] = NULL;
// $config['sess_save_path'] = sys_get_temp_dir();
// $config['sess_save_path'] = BASEPATH  . 'cache/';

$config['sess_match_ip'] = FALSE;
$config['sess_time_to_update'] = 300;
$config['sess_regenerate_destroy'] = FALSE;

$config['cookie_prefix']	= 'mc_';
$config['cookie_domain']	= '';
$config['cookie_path']		= '/';
$config['cookie_secure']	= FALSE;
$config['cookie_httponly'] 	= FALSE;

$config['standardize_newlines'] = FALSE;

$config['global_xss_filtering'] = $global_xss_filtering;
$config['csrf_protection'] = FALSE;
// $config['csrf_protection'] = TRUE;
$config['csrf_token_name'] = 'csrf_test_name';
$config['csrf_cookie_name'] = 'csrf_cookie_name';
$config['csrf_expire'] = 7200;
$config['csrf_regenerate'] = TRUE;
$config['csrf_exclude_uris'] = array();
$config['compress_output'] = $compress_output;

$config['time_reference'] = 'local';

$config['rewrite_short_tags'] = TRUE;

$config['proxy_ips'] = '$_SERVER["REMOTE_ADDR"];';