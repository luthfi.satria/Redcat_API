<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$autoload['packages'] = array();


$autoload['libraries'] = array(
							'database',
							'form_validation'
							);


$autoload['drivers'] = array();

$autoload['helper'] = array(
						"url",
						"path",
						"file",
						"myfunc",
						"authorization"
					);

$autoload['config'] = array('jwt');

$autoload['language'] = array();

$autoload['model'] = array();
$autoload['helper'][] = 'lula_helper';