<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resources_model extends CI_Model
{

    function get_config($module, $specify = array()){
        if(!empty($specify)){
            foreach ($specify as $key => $value) {
                $this->db->where($key, $value);
            }
        }

        if(is_array($module)){
            $this->db->where_in('module', $module);
        }
        else{
            $this->db->where('module', $module);
        }
        $sql = $this->db
                    ->select('id_config, module, config_key, config_label, config_value')
                    ->from('configuration')
                    ->where('is_active', 'true')
                    ->get()
                    ->result_array();

        $result = array();
        if(!empty($sql)){
            foreach ($sql as $key => $value) {
                $result[$value['config_key']] = $value['config_value'];
            }
        }
        return $result;
    }

    function get_menus($classname){
        return $this->db
                    ->select('id, parent_id, label, classname, slug, display, urutan, must_login')
                    ->from('menus')
                    ->where('classname', $classname)
                    ->get()
                    ->row_array();
    }

    function group_roles($class_id, $menus_id){
        $sql = $this->db
                    ->select('permissions')
                    ->from('usergroup')
                    ->where('group_id', $class_id)
                    ->get()
                    ->row_array();
        $roles = array();
        if(!empty($sql['permissions'])){
            $menus_id = '"'.$menus_id.'"';
            preg_match("/(?:$menus_id:\[)(.*?)(\{(.*?)\})?(?:\]+)/", $sql['permissions'], $roles, PREG_UNMATCHED_AS_NULL);
            // echo json_encode( json_decode('{'.$roles[0].'}'));die();
            $roles = !empty($roles) && is_array($roles) ? array_values((array)json_decode('{'.$roles[0].'}')) : array();
        }
        return !empty($roles) && array_key_exists(0, $roles) ? (array)$roles[0] : $roles;
    }

}
