<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_Model
{

    function index_get(){
        $notification = [
            'foto_uker' => self::__get_foto_uker()
        ];
        return $notification;
    }

    private function __get_foto_uker($data = array()){
        if(!empty($data) && array_key_exists('param', $data) && array_key_exists('key', $data)){
            $param = $data['param'];
            $key = $data['key'];
        }
        else{
            $param  = $this->input->get('param', true);
            $key    = $this->input->get('key', true);
        }

        $result = $this->db
                        ->select('kategori_foto')
                        ->from('office_picture')
                        ->where($key, $param)
                        ->where('is_verified', 1)
                        ->get()
                        ->result_array();
        return $result;
    }
}
