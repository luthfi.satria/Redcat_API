<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Users_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function getUsersInfo($data){
		return $this->db
					->select(
						'u.id, u.name, u.address, u.username, ur.redcat_store_id, u.lattitude, u.longitude,
						u.time_zone, ur.api_url'
					)
					->from('user_redcat_mapping ur')
					->join('users u','u.id = ur.id', 'inner')
					->where('ur.id',$data['id'])
					->get()
					->row_array();
	}
}