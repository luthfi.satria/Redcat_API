<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Usergroup_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function json_data(){
		$_POST = json_decode(file_get_contents("php://input"), true);		
		$this->load->library('fitable');

		$this->fitable
			 ->select('group_name, group_jabatan, is_ldap, kode_jabatan, group_id')
			 ->from('view_usergroup')
			 // ->join('office_type b','b.id_office_type = a.id_office_type')
			 ->defined_id('group_id')
			 ->fetch_assoc(true);

		$data = $this->fitable
					 ->data_type([3 => 'regex'])
			 		 ->filter_statement(array('kode_jabatan' => 'REGEXP'))
					 ->aodata();

		return $data;
	}

	public function detail_data($param = null, $flag = null){
		$param 	= !empty($param) ? $param : $this->input->get('param', true);
		$flag 	= !empty($flag) ? $flag : $this->input->get('key', true);
		$glue 	= ($flag == 'kode_jabatan') ? 'REGEXP' : '';
		$param  = ($flag == 'kode_jabatan') ? "(\"$param\")" : $param;
		$mock_data = [
			'group_name', 'group_id', 'kode_jabatan', 'set_as_default', 'permissions','group_jabatan','is_ldap'
		];

		return $this->db
					->select(implode(',', $mock_data))
					->from('view_usergroup')
					->where($flag.' '.$glue, $param)
					->get()
					->row_array();
	}

	public function add_data(){
		$data = [
			'group_name' 		=> $this->input->post('group_name', true),
			// 'id_office_type' 	=> $this->input->post('id_office_type', true),
			'kode_jabatan' 		=> $this->input->post('kode_jabatan', true) != '' ? $this->input->post('kode_jabatan', true) :substr(uniqid(), 0, 8),
			'set_as_default' 	=> $this->input->post('set_as_default', true)
		];

		if($this->input->post('group_id', true) != false){
			$data['group_id'] = $this->input->post('group_id', true); 
		}

		return $this->db 
					->insert('usergroup', $data);
	}

	public function update_data(){
		$data = [
			'group_name' 		=> $this->input->post('group_name', true),
			// 'id_office_type' 	=> $this->input->post('id_office_type', true),
			'kode_jabatan' 		=> $this->input->post('kode_jabatan', true),			
			'set_as_default' 	=> $this->input->post('set_as_default', true)
		];
		$where = ["group_id" => $this->input->post('group_id', true)];
		$update = $this->db
						->set($data)
						->where($where)
						->update('usergroup');
		return $update;
	}

	public function delete_data(){
		$where = ["group_id" => $this->input->post('group_id', true)];
		return $this->db->delete('usergroup', $where);
	}	

	public function jabatan_exists_get(){
		$data = $this->db->select('group_concat(concat(kode_jabatan)) AS kode_exists')
						 ->from('usergroup')
						 ->get()->row_array();

		return $data;
	}
}