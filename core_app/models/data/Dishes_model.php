<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Dishes_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function getDish($data){
		return $this->db
					->select(
						'id, plu'
					)
					->from('dishes')
                    ->where('created_by',$data['id'])
                    ->where_in('plu', $data['plus'])
					->get()
					->result_array();
	}
}