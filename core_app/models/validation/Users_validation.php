<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Users_validation extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function login(){
		$this->form_validation->set_rules(
			'username',
			'username',
			'required'
		);
		$this->form_validation->set_rules(
			'password',
			'password',
			'required'
		);
		return $this->form_validation->run();
	}

	function logout(){
		$this->form_validation->set_rules(
			'username',
			'username',
			'required'
		);

		return $this->form_validation->run();
	}	

	function add_data(){
		$this->load->model('validation/dictionary_validation');
		self::username_rules();
		self::password_rules();
		$this->form_validation->set_rules('email','Email', [
			'required',
			'valid_email',
			[
				'check_email',
				function($str){
					$this->load->model('data/users_model');
   					$user = $this->db
			    				 ->select('userid')
			    				 ->from('users')
			    				 ->where('email', $str)
			    				 ->where('is_active', 1)
			    				 ->where('is_ldap', 'N')
			    				 ->get()
			    				 ->row_array();					

					if(!empty($user)){
						$this->form_validation->set_message('check_email', 'email '.$str.' sudah ada, silahkan gunakan email yang lain');
						return false;
					}
					return true;
				}
			]
		]);		
		$this->dictionary_validation->required_rules('group_id', 'Group ID');
		$this->dictionary_validation->required_rules('personal_number', 'personal number');
		return $this->form_validation->run();
	}

	function index_put(){
		$this->load->model('validation/dictionary_validation');
		$this->form_validation->set_data($this->input->post());
		$this->dictionary_validation->required_id_rules('userid', 'User ID');
		self::username_rules();
		$this->dictionary_validation->email_rules('email','Email');
		$this->dictionary_validation->required_rules('group_id', 'Group ID');
		$this->dictionary_validation->required_rules('nama_karyawan', 'Nama Karyawan');
		$this->dictionary_validation->required_rules('personal_number', 'personal number');
		$this->dictionary_validation->numeric_rules('kode_branch', 'Kode Branch');
		return $this->form_validation->run();		
	}

	function delete_data(){
		$this->load->model('validation/dictionary_validation');
		$this->form_validation->set_data($this->input->post());
		$this->dictionary_validation->required_id_rules('userid', 'User ID');
		return $this->form_validation->run();
	}

	function detail_data(){
		$this->load->model('validation/dictionary_validation');
		$this->form_validation->set_data($this->input->get());
		$this->dictionary_validation->required_rules('key', 'Key Field');
		$this->dictionary_validation->required_rules('param', 'Param Field');
		return $this->form_validation->run();
	}	

	private function username_rules(){
		$this->form_validation->set_rules('username', 'Username', array(
			'required',
			'trim',
			'min_length[5]',
			'max_length[15]',
			array(
				'formatting',
				function($str){
					if(preg_match('/^[a-z0-9]+(?:[_.-][a-zA-Z0-9]+)*$/', $str) == false){
						$this->form_validation->set_message('formatting', 'Invalid username format');
						return false;
					}
					return true;
				}
			)
		));
		return;
	}

	private function password_rules(){
		$this->form_validation->set_rules('password', 'Password', array(
			'required',
			'trim', 
			'min_length[5]', 
			'max_length[15]'
		));
		return;
	}

	function change_password_put(){
		$this->form_validation->set_data($this->input->post());
		$this->form_validation->set_rules('password_lama', 'Password', array(
			'required',
			'trim', 
			'min_length[5]', 
			'max_length[15]',
			array(
				'check_password',
				function($str){
					$password = sha1(md5($str.$this->config->item('encryption_key')));
					$email = $this->input->post('email', true);
					$sql = $this->db 
								->select('password')
								->from('users')
								->where('email', $email)
								->get()
								->row_array();
					if(!empty($sql) && $password == $sql['password']){
						return true;
					}
					else{
						$this->form_validation->set_message('check_password', "Password lama tidak sesuai");
						return false;
					}
				}
			)
		));

		$this->form_validation->set_rules('password_baru', 'Password baru', 'required|min_length[5]|max_length[15]');
		$this->form_validation->set_rules('repassword_baru', 'Tulis ulang password', 'required|matches[password_baru]');
		return $this->form_validation->run();
	}
}