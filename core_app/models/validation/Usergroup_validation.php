<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Usergroup_validation extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function add_data(){
		self::group_name_rules();
		self::kode_jabatan_rules();
		self::setupas_rules();
		return $this->form_validation->run();
	}

	function update_data(){
		$this->form_validation->set_data($this->input->post());
		self::id_rules();
		self::group_name_rules();
		self::kode_jabatan_rules();
		self::setupas_rules();
		return $this->form_validation->run();
	}

	function delete_data(){
		$this->form_validation->set_data($this->input->post());
		self::id_rules();
		return $this->form_validation->run();
	}

	function detail_data(){
		$this->form_validation->set_data($this->input->get());
		$this->form_validation
			 ->set_rules('key', 'Key Search', 'required')
			 ->set_rules('param', "Parameter", "required");
		return $this->form_validation->run();
	}

	private function id_rules(){
		$this->form_validation->set_rules(
			'group_id',
			'ID group',
			'required|integer|trim'
		);
		return;		
	}

	private function group_name_rules(){
		$this->form_validation->set_rules(
			'group_name',
			'group name',
			array(
				'required',
				'trim',
				'min_length[5]',
				'max_length[30]',
				array(
					'check_redundant',
					function($str){
						$kode_jabatan = $this->input->post('kode_jabatan', true);
						$sql = $this->db
									->select('group_name')
									->from('usergroup')
									->where('group_name', $str)
									->where('kode_jabatan', $kode_jabatan)
									->get()
									->row_array();
						if(empty($sql)){
							return true;
						}
						elseif (count($sql) == 1 && $str == $sql['group_name']) {
							return true;
						}

						$this->form_validation->set_message('check_redundant', 'group name already exists');
						return false;
					}
				)
			)
		);
		return;		
	}

	private function kode_jabatan_rules(){
		$this->form_validation
			 ->set_rules('kode_jabatan', 'Kode Jabatan', 'required');
		return;
	}

	private function setupas_rules(){
		$this->form_validation->set_rules(
			'set_as_default',
			'set_as_default',
			'required|in_list[0,1]|trim'
		);
		return;
	}
}