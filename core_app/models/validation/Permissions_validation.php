<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Permissions_validation extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	private function group_id_rules(){
		$this->form_validation->set_rules(
			'group_id', 
			'group ID',
			array(
				'required',
				'trim',
				'integer'
			)
		);
		return;
	}

	public function active_menu_post(){
		$this->form_validation->set_data(
			array('group_id' => $this->input->post('group_id', true))
		);
		self::group_id_rules();
		return $this->form_validation->run();
	}

	public function index_post(){
		$this->form_validation->set_data(
			['group_id' => $this->input->post('group_id', true)]
		);
		self::group_id_rules();
		return $this->form_validation->run();
	}

	function index_put(){
		$this->load->model('validation/dictionary_validation');
		$this->form_validation->set_data($this->input->post());
		$this->dictionary_validation->required_id_rules('id', 'menus ID');
		$this->dictionary_validation->required_rules('label', 'label menus');
		$this->dictionary_validation->required_rules('slug', 'slug menus');
		$this->dictionary_validation->numeric_rules('urutan', 'order number');
		$this->dictionary_validation->inlist_rules('must_login', 'login required', '0,1');
		return $this->form_validation->run();
	}
}