<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
/*
* --------------------------------------------------------------------------
* CUSTOM FUNCTION
* --------------------------------------------------------------------------
*/
    function mycaptcha($page = '', $slug=''){
      $ci = &get_instance();
      $ci->load->helper('captcha');
      $panel = !empty($GLOBALS['lookup']) && $GLOBALS['lookup'] == true ? BASE_PANEL : '';
      // $random_value = array_merge(range('a', 'z'),range('A', 'Z'),range(0, 9));
      $random_value = range(0,9);
      $random_value = implode('', $random_value);
      $random_value = str_shuffle($random_value);
      $random_value = substr($random_value, 2, 5);
      $vals = array(
        'word'          => $random_value,
        'img_path'      => CAPTCHAPATH,
        'img_url'       => base_url(MINIFIER.'assets/captcha'),
        'font_path'     => FCPATH.'assets/fonts/Roboto-Bold.ttf',
        'img_width'     => 250,
        'img_height'    => 80,
        'expiration'    => 7200,
        'word_length'   => 30,
        'font_size'     => 36,
        'img_id'        => $page.'-captcha',
        'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

        'colors'        => array(
            'background' => array(224, 224, 224),
            'border' => array(255, 255, 255),
            'text' => array(0,0,0),
            'grid' => array(42, 63, 84)
        )
      );

      $folder = glob(CAPTCHAPATH.'*');
      foreach ($folder as $key => $value) {
        if(is_file($value))
            unlink($value);
      } 

      $cap = create_captcha($vals);
      $ci->session->set_userdata(DEF_APP.$slug.'captcha_word', $cap['word']);
      $ci->session->set_userdata(DEF_APP.$slug.'captcha_time', $cap['time']);
      return $cap['image'];     
    }
    
    function ago($time)
    {
       $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
       $lengths = array("60","60","24","7","4.35","12","10");

       $now = time();

           $difference     = $now - $time;
           $tense         = "ago";

       for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
           $difference /= $lengths[$j];
       }

       $difference = round($difference);

       return "$difference $periods[$j] ago";
    }

    function html_parser($string){
        $search = array (
            '/([<]table\s.*?[>])/',
            '/([<]\/table[>])/'
          );

          $replace = array (
            '<div class="table-responsive"><table class="table table-striped">',
            '</table></div>',
          );
      $output = preg_replace($search, $replace, $string);
      return $output;
    }

    function convert_date($string){
        $find = array(
                    "sunday","monday","tuesday","wednesday","thursday","friday","saturday",
                    "january","february","march","april","may","june","july","august","september","october","november","december",
                    "second", "minute", "hour", "day", "week", "month", "year", "decade","ago"
                );

        $replace = array(
                    "minggu","senin","selasa","rabu","kamis","jum'at","sabtu",
                    "januari","februari","maret","april","mei","juni","juli","agustus","september","oktober","november","desember",
                    "detik", "menit", "jam", "hari", "minggu", "bulan", "tahun", "dekade","lalu"
                );

        $convert = str_replace($find, $replace, strtolower($string));
        return ucwords($convert);        
    }

    function local_date($string){
      return date(LDATE, strtotime($string));
    }

    function currency($string, $digits = null){
        $digits = !empty($digits) ? $digits : DEC_DIGIT;
        return number_format($string,$digits,DEC_SEP,THOU_SEP);
    }
        
    function currency_reverse($string){
        $string = explode(DEC_SEP, $string);
        return str_replace(array(THOU_SEP,DEC_SEP), '', $string[0]);
    }

    function seo_reverse($string){
        return str_replace(array('-'), ' ', $string);
    }
    
    function parse_link(){
      $array = func_get_args();
      $array = array_map(function($items){
          if(preg_match('/(\/)/', $items) == false){
            return seo_url($items);
          }
          else{
            return $items;
          }
      },$array);
      return site_url(implode('/',$array));
    }

    function randcolor(){
      $rand = array('5D9CEC','4A89DC','37BC9B','F6BB42','DA4453','AAB2BD','D24D57','336E7B','D770AD');
      $color = '#'.$rand[rand(0,8)];
      return $color;
    }

    function encrypt_str($string){
        $this->load->library('encryption');
        return $this->encryption->encrypt($string);
    }

    function decrypt_str($string){
        $this->load->library('encryption');        
        return $this->encryption->decrypt($string);
    }

    function valid_date($str){
        if(preg_match('/^(\w{1})-(\w{1})-(\w{1})$/', $str) == false){
             $this->form_validation->set_message('valid_date', 'Field %s tidak valid!, format tanggal harus  yyyy-mm-dd');
             return FALSE;
        }
        else{
            return TRUE;
        }
    }
    function valid_currency($str){
        if(preg_match('/^((?:\d{1,3}[,\.]?)+\d*)$/', $str) == false){
             $this->form_validation->set_message('valid_currency', 'Field %s tidak valid!, format ribuan dipisah tanda (,)');
             return FALSE;
        }
        else{
            return TRUE;
        }
    }
    function timeformat($str){
        if(preg_match('/^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$/', $str) == false){
             $this->form_validation->set_message('timeformat', 'Field %s tidak valid!, format waktu (00:00:00)');
             return FALSE;
        }
        else{
            return TRUE;
        }
    }    
    function request_checker(){
        if($_SERVER['REQUEST_METHOD'] != "POST"){
            redirect(404);
        }
    }

    function linked($string, $optional=''){
      $optional = !empty($optional) ? '/'.$optional : '';
      $string = seo_url($string);
      preg_match('/((.*)'.$string.')[^\/]/', $GLOBALS['route'], $matches);
      return site_url(trim($matches[0]).$optional); 
    }

    function seo_url($string){
        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        return $string;
    }

    function kamus_bahasa($array, $key, $flag){
        return $array[$key][$flag];
    }

    function post_curl($postdata, $url = '', $key = array("Api-Key: dhiDH@AWIAWDWd&7wd7q8wqyddwjqkhq67612836178sdqydq=")){
      $curl   = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
      curl_setopt($curl, CURLOPT_POST, true);
      if(!empty($postdata)){
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postdata));
      }
      curl_setopt($curl, CURLOPT_HTTPHEADER, $key);

      $response = curl_exec($curl);
      $response = json_decode($response);

      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        return array('status' => 500, "error" => $err);
      } else {
        return $response;
      }
    }

    function create_token($value)
    {
      $keyspace = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $str = '';
      $max = mb_strlen($keyspace, '8bit') - 1;
      for ($i = 0; $i < $value; ++$i) {
        $str .= $keyspace[random_int(0, $max)];
      }
      return $str;
    }