<?php
Header('Access-Control-Allow-Origin: *'); 
Header('Access-Control-Allow-Headers: Origin ,X-Requested-With ,Content-Type ,Accept ,Accept-Encoding ,Accept-Language ,Access-Control-Request-Method ,Access-Control-Allow-Origin ,Access-Control-Request-Headers ,Authorization'); 
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
Header('Content-Type: application/json; charset=utf-8');

defined('BASEPATH') OR exit('No direct script access allowed');

use Google\Client;
use Google\Service;
use Aws\Exception\AwsException;

class Images extends Admin_Controller {

	function __construct(){
		parent::__construct();
		// Header('Access-Control-Allow-Origin: *'); 
		// Header('Access-Control-Allow-Headers: Origin ,X-Requested-With ,Content-Type ,Accept ,Accept-Encoding ,Accept-Language ,Access-Control-Request-Method ,Access-Control-Allow-Origin ,Access-Control-Request-Headers ,Authorization'); 
		// Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
	}

	public function catching_request_post(){
		$data = json_decode($this->input->raw_input_stream, true);
		// get user details
		$this->load->model([
			'data/users_model',
			'data/dishes_model'
		]);

		$request['users_data'] = $this->users_model->getUsersInfo($data);
		
		$dish = $this->dishes_model->getDish($data);
		$request['image_plus'] = !empty($dish) ? array_column($dish, 'id', 'plu') : [];

		$request['parameters'] = $data['parameters'];
		SELF::start_sync($request);
	}

	public function checking_db_post(){
		$response = [
			'ENV' => ENVIRONMENT,
			'server' => $_SERVER,
			'staging_conf' => is_file(APPPATH.'config/staging/database.php') ? 
			file_get_contents(APPPATH.'config/staging/database.php') : '',
			'dbconf' => $this->db
		];
		SELF::response_ok($response);
	}

	public function start_sync($data){
		if(!empty($data)){
			$param = $data['parameters'];

			$imagePath = $data['users_data']['api_url'];
			$imagePath = parse_url($imagePath);
			$imagePath = !empty($imagePath) ? $imagePath['scheme'].'://'.$imagePath['host'] : '';
			$date = new DateTime();
			$version =  $date->getTimestamp();

			if(!empty($imagePath)){
				$imagePath .= '/static/img/ordering/';
			}

			unset($data['parameters']);

			$uploads_dir = UPLOAD_PATH.'uploads/original';
			$itemData = $defaultData = [];

			$opts = array(
				'http' => [
					'protocol_version' => 1.1
				],
				'ssl' => [
					"verify_peer"=>false,
					"verify_peer_name"=>false
				]
			);
			
			$context = stream_context_create($opts);
			
			foreach($data['image_plus'] as $key => $value){
				// $imagePath = 'http://redgate.at.org/images/symbols/thumbs/'; // for testing only
		
				$redcat_request = [
					'url' => $data['users_data']['api_url'].'/'.$key,
					'method' => 'GET'
				];
				
				// getting details item
				$details_item = self::getRequestData($redcat_request);
				
				// define image name
				$imageName = '';
				$originalName = rawurlencode($details_item->data->ImageLarge);
				if(!empty($details_item->data->ImageLarge)){
					$imageName .= $data['users_data']['id'].'_'.$value.'_dish';
					$imageName .= '_'.str_replace(' ','_',$details_item->data->ImageLarge);
					// $imageName = $key.'.png'; // for trial purpose
				}

				// define image path
				if(!empty($details_item->data->ImagePath)){
					$imagePath = $details_item->data->ImagePath;
				}

				// copy image from URL(s)
				if(!is_dir($uploads_dir)){
					mkdir($uploads_dir, 0777, true);
				}
				
				if(!empty($imageName) && !empty($imagePath)){
					$getImgSize = $this->_httpRequests([
						"url" => $imagePath.$originalName,
						"method" => 'GET',
						"connect_timeout" => 10,
						"SSL" => false,
						"return_transfer" => true
					]);
					
					if(!empty($getImgSize)){
						copy($imagePath.$originalName, $uploads_dir.'/'.$imageName, $context);
						$data['image_path'][] = $uploads_dir.'/'.$imageName;
						$itemData[] = [
							'id' => $value,
							'image' => $imageName
						];
					}
					else{
						$defaultData[] = [
							'id' => $value,
							'image' => 'default-'.$data['users_data']['id'].'.png'
						];
					}
				}
				else{
					$defaultData[] = [
						'id' => $value,
						'image' => 'default-'.$data['users_data']['id'].'.png'
					];					
				}
			}
			
			// Update item data
			if(!empty($itemData)){
				$this->load->model('data/image_model');
				$updated_item = $this->image_model->updateItem($itemData);
				
				// resizing image
				self::_image_resizing($data, $param);
			}

			if(!empty($defaultData)){
				if($this->load->is_loaded('data/image_model') == false){
					$this->load->model('data/image_model');
				}
				$updated_item = $this->image_model->updateItem($defaultData);
			}

			// handling for upload
			
			// if($param['IS_AWS'] == "TRUE"){
			if(in_array($param['IS_AWS'], [1, TRUE, "TRUE", 'true']) == true){
				$response = self::_imageHandlerForAWS($data, $param);
			}
			else{
				$response = self::_imageHandlerForGoogle($data, $param);
			}

			// end sync image process
			// set logging value = 0
			self::_loggingProcess(0);
			self::response_ok($response);
		}
		else{
			self::response_failed(SELF::HTTP_BAD_REQUEST,'Invalid post data format');
		}
	}

	/**
	 * RESIZING IMAGES
	 */

	function _image_resizing($data, $param){
		$this->load->library('image_lib');
		$copying_dir = UPLOAD_PATH.'uploads/';

		// image lib configuration
		$config['image_library'] = 'gd2';
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['thumb_marker'] = '';

		// image copy folder
		$folder = [
			'small' => [
				"width" => 110,
				"height" => 110
			],
			'medium'=> [
				"width" => 226,
				"height" => 226
			],
			'large'=> [
				"width" => 640,
				"height" => 960
			],
			'smartweb' => [
				"width" => 500,
				"height" => 500
			],
		];

		foreach($data['image_path'] as $key => $value){
			$imgInfo = getimagesize(APPPATH.'../'.$value);
			
			// vertical image
			if($imgInfo[1] <= $imgInfo[0]){
				// we using width size as calculation
				$ratio = $imgInfo[1] / $imgInfo[0];
				$primary = 'width';
			}
			// horizontal image
			else{
				// we using height as  calculation
				$ratio = $imgInfo[0] / $imgInfo[1];
				$primary = 'height';
			}
			
			$imageName = explode('/',$value);
			$imageName = !empty($imageName) ? end($imageName) : '';

			foreach($folder as $fk => $fv){
				$config['source_image'] = $value;
				if($primary == 'width'){
					$config['width']        = $fv[$primary];
					$config['height']       = $ratio * $fv[$primary];
				}
				else{
					$config['width']        = $ratio * $fv[$primary];
					$config['height']       = $fv[$primary];
				}

				$config['new_image']	= $copying_dir.$fk;

				// copy image from URL(s)
				if(!is_dir($config['new_image'])){
					mkdir($config['new_image'], 0777, true);
				}

				$config['new_image'] .= '/'.$imageName;
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
			}
			
			unlink($value);	
		}
	}
	
	function checking_aws_post($deleteObj = false){
		if($this->load->is_loaded('awslib') == false){
			$this->load->library('awslib');
		}
		
		$param = json_decode($this->input->raw_input_stream, true);
		$test = $this->awslib
				->credentials('ap-southeast-1', $param['S3_PUBLIC_KEY'], $param['S3_SECRET_KEY'])
				->bucketName($param['IMG_BUCKET'])
				->list_objects()
				->toArray();
		if($deleteObj == true && array_key_exists("Contents", $test) == true){
			foreach($test['Contents'] as $key => $value){
				$this->awslib->delete_object($value['Key']);
			}
		}
		self::response_ok($test);
	}

	/**
	 * UPLOADING IMAGE INTO CLOUD STORAGE
	 * 3621_480473_dish_Artboard_87
	 */
	function _imageHandlerForAWS($imageInfo = null, $param = null){
		$this->load->helper('directory');
		$this->load->library('awslib');

		$copying_dir = UPLOAD_PATH.'uploads/';
		$map = directory_map(UPLOAD_PATH);
		if(!empty($map)){
			$this->awslib
				 ->credentials('ap-southeast-1', $param['S3_PUBLIC_KEY'], $param['S3_SECRET_KEY'])
				 ->bucketName($param['IMG_BUCKET']);

			foreach($map['uploads/'] as $key => $value){
				if(!empty($value) && is_array($value) && $key != 'original/'){
					foreach($value as $fk => $fv){
						self::_loggingProcess(1);
						$mime = get_mime_by_extension($copying_dir.$key.$fv);
						$content = file_get_contents($copying_dir.$key.$fv, true);
						
						try{
							$obj = $this->awslib
										->get_object($key.$fv);

							if($obj){
								$this->awslib->delete_object($key.$fv);
							}
						}
						catch(AwsException $err){
						}

						// insert new image
						try{
							$put = $this->awslib->put_object([
								"Key" => $key.$fv,
								"Body" => $content
							]);
							unlink($copying_dir.$key.$fv);
							self::_loggingProcess(0);
						}
						catch(AwsException $err){
							$error[]=[
								'images' => $key.$fv,
								'method' => 'put',
								'error' => $err->getAwsErrorCode()
							];
							unlink($copying_dir.$key.$fv);
							self::_loggingProcess(0);
						}
					}
				}
			}
			// return true;
		}
		// return false;
		return !empty($error) ? $error : true;
	}

	function _imageHandlerForGoogle($imageInfo = null, $param = null){
		$this->load->helper('directory');
		$copying_dir = UPLOAD_PATH.'uploads/';
		$map = directory_map(UPLOAD_PATH);

		if(!empty($map)){
			$client = new Google_Client();
			// $client->setAuthConfig(APPPATH.'config/tabsquare-805d0-c26db15c941b.json');
			$client->setAuthConfig(APPPATH.'config/google_credential.json');
			$client->useApplicationDefaultCredentials();
			$client->addScope(Google_Service_Storage::DEVSTORAGE_FULL_CONTROL);
			$storage = new Google_Service_Storage($client);
			$obj = new Google_Service_Storage_StorageObject();

			foreach($map['uploads/'] as $key => $value){
				if(!empty($value) && is_array($value) && $key != 'original/'){
					foreach($value as $fk => $fv){
						self::_loggingProcess(1);

						$mime = get_mime_by_extension($copying_dir.$key.$fv);
						$content = file_get_contents($copying_dir.$key.$fv, true);

						$body = array(
							'name' => $key.$fv,
							'data' => $content,
							'uploadType' => 'multipart',
							'mimeType' => $mime,
							'predefinedAcl' => 'publicread'
						);
	
						// delete existing image
						try{
							$originalObject = $storage->objects->get($param['GCS_BUCKET'], $key.$fv);
							if($originalObject) {
								$del = $storage->objects->delete($param['GCS_BUCKET'],$key.$fv);
							}
						}
						catch(Exception $err){

						}

						// insert new image
						$put = $storage->objects->insert($param['GCS_BUCKET'], $obj, $body);
						unlink($copying_dir.$key.$fv);
						self::_loggingProcess(0);
					}
				}					
			}
			return true;
		}
		return false;
		// it's working now
		// $client = new Google_Client();
		// $client->setAuthConfig(APPPATH.'config/tabsquare-805d0-c26db15c941b.json');
		// $client->useApplicationDefaultCredentials();
		// $client->addScope(Google_Service_Storage::DEVSTORAGE_FULL_CONTROL);
		
		// $storage = new Google_Service_Storage($client);
		// $originalObject = $storage->objects->listObjects($param['GCS_BUCKET'], array());
		// echo json_encode($originalObject);die();
	}

	/**
	 * LOGGING
	 */
	function _loggingProcess($logStatus = 1){
		$logPath = APPPATH.'docs/logging_process.txt';
		if(is_file($logPath)){
			write_file($logPath, $logStatus);
		}
		return true;
	}

	function logging_status_get(){
		$logPath = APPPATH.'docs/logging_process.txt';
		if(is_file($logPath)){
			$content = read_file($logPath);
			self::response_ok($content);
		}		
	}

	/**
	 * HTTP REQUEST HANDLER
	 */
    private function getRequestData($data){
        $curl   = curl_init();
        curl_setopt($curl, CURLOPT_URL, $data['url']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		// curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);

        if(!empty($data['header'])){
            curl_setopt($curl, CURLOPT_HTTPHEADER, $data['header']);
        }

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $data['method']);
        
        if(!empty($data['data'])){
          curl_setopt($curl, CURLOPT_POSTFIELDS, $data['data']);
        }

        $response = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if($code == 400 || $code == 404 || $code == 403){
          $response = array(
            "status" => false,
            "code" => $code,
            "message" => $code != 403 ? 'Page ['.$data['url'].'] Is Not Found' : 'Access forbidden',
            "feedback" => curl_getinfo($curl)
          );
        }
        else{
          $response = json_decode($response);
        }

        curl_close($curl);
        return $response;
    }

    private function _httpRequests($data){
        $returnTransfer = array_key_exists('return_transfer', $data) ? $data['return_transfer'] : true;
        $timeOut = array_key_exists('timeout', $data) ? $data['timeout'] : 30;

        $curl   = curl_init();
        curl_setopt($curl, CURLOPT_URL, $data['url']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, $returnTransfer);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1 );
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeOut);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_HEADER, 0);

        if(array_key_exists('connect_timeout', $data)){
          curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $data['connect_timeout']);
        }

        if(array_key_exists('SSL', $data)){
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $data['SSL']);
        }

        $response = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if($code !== 200){
          return false;
        }
        else{
            $image = imagecreatefromstring( $response );
            $response = [ "width" => imagesx( $image ), "height" => imagesy( $image ) ];
            imagedestroy($image);
        }

        curl_close($curl);

        return $response;
    }	
}
