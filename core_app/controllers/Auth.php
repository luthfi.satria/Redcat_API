<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
        $model = array(
            'data/users_model',
            'validation/users_validation'
        );
        $this->load->model($model);
    }

    public function index_get(){
        self::response_failed(SELF::HTTP_BAD_REQUEST, 'Not Found');
    }

    public function token_post(){
        $_POST = json_decode($this->input->raw_input_stream, true);
        if($this->users_validation->login() == true){
            $login = $this->users_model->login();
            if(!empty($login)){
                // generate token
                $jwt = $this->users_model->generate_token($login);
                $callable = array(
                    'status'        => true,
                    'code'          => SELF::HTTP_OK,
                    'group_id'      => $jwt['group_id'],
                    'access_token'  => $jwt['token'],
                );

                // store to redis
                if($this->input->post('phpunit') == false){
                    $store = $this->users_model->store_token($jwt);
                    if(!$store){
                        $callable = array(
                            'status' => false,
                            'code'   => SELF::HTTP_BAD_REQUEST,
                            'message' => 'Unable to store token, redis is not supported'
                        );
                    }                
                }
                
                $this->set_response($callable, $callable['code']);
                return;
            }
            else{
                self::response_failed(
                    SELF::HTTP_BAD_REQUEST,
                    'Wrong username and/or password'
                );
            }
        }

        self::response_failed(
            SELF::HTTP_BAD_REQUEST, 
            'Validation Error', 
            ['error' => $this->form_validation->error_array()]
        );
    }
}
