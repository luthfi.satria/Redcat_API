<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Migrate extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	function index_get(){
		$this->load->library('migration');
		if($this->migration->current() == false){
			self::create_log('migration', $this->userdata->username, 'Migrate Databases');
			// show_error($this->migration->error_string());
			self::response_failed(
				SELF::HTTP_INTERNAL_SERVER_ERROR,
				show_error($this->migration->error_string())
			);
		}

		self::response_ok(true);
	}

	function refractor_uker_post(){
		$this->load->model('data/office_model');
		$sql = $this->office_model->list_office();
		$sql = self::refractoring($sql);
		$sql = $this->office_model->bulk_update($sql);
	}

	private function refractoring($data, $result = array()){
		$indexes = array_column($data, 'office_slug', 'id_office');
		foreach ($data as $key => $value) {
			$slug = !empty($indexes) && array_key_exists($value['parent_office'], $indexes) ? 
				$indexes[$value['parent_office']].$value['parent_office'].':' : '1:';
			if($key != 0){
				$data[$key]['office_slug'] = $indexes[$value['id_office']] = $slug;
			}
		}
		return $data;
	}

	private function old_refractoring($data, &$result = array(), $last_row = array()){
		$i = 1;
		foreach ($data as $key => $value) {
			if(!empty($value['office_slug'])){
				$office_slug = $value['office_slug'];
			}
			elseif (!empty($result) && array_key_exists($value['parent_office'], $result)) {
				if($last_row == $value['parent_office']){
					$i++;
				}
				else{
					$i = 1;
				}
				$office_slug = $result[$value['parent_office']]['office_slug'].$i.":";
			}
			else{
				$i = 1;
			}

			$result[$value['id_office']] = array(
				"id_office" 		=> $value['id_office'],
				"parent_office" 	=> $value['parent_office'],
				"id_office_type" 	=> $value['id_office_type'],
				"kode_branch" 		=> $value['kode_branch'],
				"parent_branch" 	=> $value['parent_branch'],
				"office_name" 		=> $value['office_name'],
				"office_slug" 		=> $office_slug,
			);

			$last_row = $value['parent_office'];
		}
		return $result;
	}
}
