<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Notification extends Admin_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$model = array(
			'main_model'
		);
		$this->load->model($model);
	}

	function index_get(){
		$_GET = json_decode($this->input->raw_input_stream, true);
		$response = $this->main_model->index_get();
		self::response_ok('OK',$response);
	}
}
