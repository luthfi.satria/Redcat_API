<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/


class Api_Controller extends FI_Controller
{

    function __construct()
    {
        parent:: __construct(); 
        libxml_disable_entity_loader(TRUE);
        if (is_php('5.4') === FALSE)
        {
            // CodeIgniter 3 is recommended for v5.4 or above
            throw new Exception('Using PHP v' . PHP_VERSION . ', though PHP v5.4 or greater is required');
        }

        // Check to see if this is CI 3.x
        if (explode('.', CI_VERSION, 2)[0] < 3)
        {
            throw new Exception('REST Server requires CodeIgniter 3.x');
        }               
    }

    function initialize(){
     
        $whitelist  = array();
        $httpheader = getallheaders();

        $key = array_keys($httpheader);
        return true;
    }

    function whitelist(){
        $whitelist = array(
            'devhouseApps01' => 'NjZhYzFiZTNhZDI2Yzc3MGUyMWVkOGUwNGQzNzFlMzJkZDBkNTZmNw=='
        );
        return $whitelist;
    }

    function security_key_check(){
        $sec_key = $this->input->post('security_key', true);
        if(!in_array($sec_key, array_values(self::whitelist()) )){
            $output = array(
                'code' => 400,
                'description' => 'invalid security key'
            );
            $this->outputs($output, 'application/json');
        }
    }
    
    /* ------------------------------------------------------------------------------------------------------
    ** API HANDLER
    ** ------------------------------------------------------------------------------------------------------
    */ 

    function checking_token(){
        $this->load->model('main_model');
        $tokenizer = $this->main_model->checking_token();
        if($tokenizer == false){
            $output = array(
                'success'   => false,
                'message'   => 'Invalid session token'
            );
            $this->outputs($output, 'application/json');
            die();
        }        
    }

    function load_func_exec(){
        // return self::check_api_key();
        return true;
    }                           

    function check_api_key(){
        $this->load->helper('myfunc');
        $whitelist  = array();
        $httpheader = function_exists('getallheaders') == true ? getallheaders() : getall_headers();
        return true;           
    }

    /*******************************************************************************************************
    # FUNGSI http_access
    # TUJUAN : Memvalidasi user sebelum aktifitas melihat, menambahkan, mengedit, atau menghapus data
    # LIBRARY : -
    # HELPER : -
    # REF MODEL : data/admin_model, validation/valid_admin
    # ---------------------------------------------------------------------------------------------------
    # ACCESS PARAMETER :
    # ---------------------------------------------------------------------------------------------------
    # @integer id_user
    # @string username
    # @integer id_group
    # @string token
    ******************************************************************************************************/ 

    protected function http_access(){
        $langtype = array('validation');
        $this->load->model('validation/valid_admin');
        $this->kamus = $this->api_model->kamus($langtype);
        // 1. Validasi 
        // 1.1 validasi user dan akses token
        if($this->valid_admin->http_access() == true){
            // 2. Cek user data dan akses token
            $this->load->model('data/admin_model');
            $user = $this->admin_model->is_admin();
            // 3. Jika user tersebut terdapat di database, maka berikan persilahkan untuk melanjutkan proses lainnya
            if(!empty($user)){
                return true;
            }
            // 3.1 Sebaliknya, tampilkan pesan galat ~ key: access_denied
            else{
                $output = array(
                    'code'      => '400',
                    'message'   => 'Akses ditolak'
                );
                $this->response($output, $output['code']);
            }
        }
        // 1.2 Tampilkan pesan galat ~ key:access_denied
        else{
            $output = array(
                'code' => '400',
                'message' => 'Akses ditolak',
                'error' => $this->form_validation->error_array()
            );
            $this->response($output, $output['code']);
        }
    }

    /*******************************************************************************************************
    # FUNGSI control_priviledge
    # TUJUAN : Menyangkal user terhadap metode yang dilakukan seperti dapat melihat, menambah, mengedit, menghapus atau mencetak data
    # LIBRARY : -
    # HELPER : file
    # REF MODEL : validation/valid_admin
    # ---------------------------------------------------------------------------------------------------
    # PARAMETER :
    # ---------------------------------------------------------------------------------------------------
    # @integer id_group
    # @string method_name, opsi: view, insert, update, delete, printed
    ******************************************************************************************************/ 
    protected function control_priviledge($control_name, $method_name){
        $id_group = $this->input->post('access_group_id', true);       
        $fpath = GSTORAGE.'access_control/'.$id_group.'.json';
        if(file_exists($fpath)){
            $this->load->helper('file');
            $content = json_decode(read_file($fpath));
            if($content->$control_name->$method_name != 1){
                $output = array(
                    'code' => '200',
                    'message' => 'Sorry, you don\'t have priviledge to '.$method_name.' data ',
                );
                $this->response($output, $output['code']);
            }
        }
        else{
            $output = array(
                'code' => '200',
                'message' => 'Your priviledge is unset, please contact administrator',
                'storage' => $fpath
            );
            $this->response($output, $output['code']);
        }
    }
    
    function invalid_response(){
        return $output = array(
                'code' => 200,
                'message'=> 'Validation error',
                'error' => $this->form_validation->error_array(),
            );
    }

    function outputs($page, $mime = 'text/html', $buffer = true){
        $this->myhook
             ->mime($mime)
             ->charset('utf-8')
             ->status(200)
             ->expires('-10 year')
             ->cache(0)
             ->header("Accept-Ranges: bytes")
             ->header("HTTP/1.0 200 OK")
             ->header("HTTP/1.1 200 OK")
             ->header("Cache-Control: no-cache, no-store, must-revalidate", false)
             ->header("Cache-Control: post-check=0, pre-check=0", false)
             ->header('Access-Control-Request-Headers: X-Requested-With, accept, content-type')
             // ->header('Access-Control-Allow-Methods: GET, POST, OPTIONS')
             // ->header('Access-Control-Allow-Origin: *')
             // ->header('Access-Control-Allow-Origin: http://vave.co.id')
             // ->header('Access-Control-Allow-Headers: Api-Key')             
             ->header("Pragma: no-cache")
             ->header("X-Content-Type-Options: nosniff")
             ->header("X-XSS-Protection: 1; mode=block")
             ->header("X-Frame-Options: SAMEORIGIN")
             ->output($page)
             ->buffering()
             ->display();        
    }
         
}