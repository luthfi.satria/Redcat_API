<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
use Firebase\JWT\JWT;
class Admin_Controller extends FI_Controller
{
    protected $__token;
    var $userdata = '';
    var $roles;

    function __construct(){
        parent:: __construct();
        self::authorization();
        // self::roles_permissions();
    }

    /* 
     # AUTHORIZATION
     * Melakukan pengecekan token secara komprehensif
     * Pengecekan request header authorization
     * Pengecekan masa kadaluarsa token
     * Pengecekan token dengan data yang ada di redis atau file
    */
        
    public function authorization(){
        // check request header
        $this->__token = $token = self::check_request_header();

        // check token
        // $storage = self::checking_token($token); 

        // check storage
        // self::check_storage($token);

    }

    private function check_request_header(){
        $headers = $this->input->get_request_header('Authorization');
        if(empty($headers)){
            self::response_failed(SELF::HTTP_UNAUTHORIZED);
        }
        $headers = str_replace('Bearer ', '', $headers);
        // $headers = explode(' ', $headers);
        // if($headers[0] != "Bearer"){
        //     self::response_failed(SELF::HTTP_UNAUTHORIZED);
        // }
        return $headers;
    }

    private function checking_token($token){ 
        try{
            $decode = JWT::decode($token, ENCFK, array('HS256'));
            $this->userdata = $decode;
            return $token;
        }
        catch(Exception $e){
            self::response_failed(
                SELF::HTTP_UNAUTHORIZED,
                $e->getMessage()
            );
        }
    }

    private function check_storage($token){
        if($this->predis_lib->ping() != false){
            $data = $this->predis_lib->get('jwt:'.substr($token, -6));

            if(!empty($data)){
                $skr = strtotime('now');
                $data = json_decode($data);
                if($data->exp >= $skr && $data->refresh >= $skr){
                    return true;
                }
                // else{
                //     self::response_failed(SELF::HTTP_UNAUTHORIZED);
                // }
            }
            // else{
                self::response_failed(SELF::HTTP_UNAUTHORIZED, 'Request ditolak', ['error' => 'Token telah kadaluarsa']);
            // }
        }
        else{
            // $path = APPPATH.'priviledges/login/'.$decode->id.'.json';
            // if($handle      = file_get_contents($path)){
            //     $json_data  = utf8_encode($handle);
            //     if($json_data != $token){
                    self::response_failed(SELF::HTTP_SERVICE_UNAVAILABLE, 'Server sedang mengalami gangguan');
            //     }
            //     else{
            //         self::check_token_expires($json_data);
            //     }
            // }
        }
    }

    function check_priviledge($data){
        $path = APPPATH.'/priviledges/access_menu/'.$data->usergroup.'.json';
        if($users_controls = file_get_contents($path)){
            $json_data = utf8_encode($users_controls);
            $json_data = json_decode($json_data);
            self::force_response($json_data, HTTP::OK);
        }
        self::response_failed(SELF::HTTP_UNAUTHORIZED);
    }
    /*
    * FULLSTACK ENVIRONMENT
    */
    function restrict(){
        $class                  = strtolower(get_called_class());
        $restrict               = $this->resources_model->restrict($class, SESS_ADMIN);

        $this->vars['_TITLE']    = $restrict['title'];
        $this->vars['_ROUTED']   = BASE_PANEL.$restrict['slug'];
        $this->vars['_INSERT']   = $restrict['inserting'];
        $this->vars['_UPDATE']   = $restrict['updating'];
        $this->vars['_DELETE']   = $restrict['deleting'];
        $this->vars['_VIEWING']   = $restrict['viewing'];
        $this->vars['_PRINTING']   = $restrict['printing'];
    }

    function restrict_msg(){
        return $this->load->view('errors/restrict_access');
    }

    /* ------------------------------------------------------------------------------------------------------
    ** TEMPLATE INDEX
    ** ------------------------------------------------------------------------------------------------------
    */
    function master_page($hooking = true){

        if(!isset($this->vars['_TITLE'])){
            $this->vars['_TITLE'] = ucwords($this->vars['_INFO_TITLE']);
        }

        $page = $this->load->view('template/'.TEMPLATE.'/index',$this->vars,$hooking);
        
        if($hooking == true){
            self::outputs($page);    
        }
    }

    function outputs($page, $mime = 'text/html', $buffer = true){
        $this->myhook
             ->mime($mime)
             ->charset('utf-8')
             ->status(200)
             ->expires('-10 year')
             ->cache(0)
             ->header("Accept-Ranges: bytes")
             ->header("HTTP/1.0 200 OK")
             ->header("HTTP/1.1 200 OK")
             ->header("Cache-Control: no-cache, no-store, must-revalidate", false)
             ->header("Cache-Control: post-check=0, pre-check=0", false)
             ->header("Pragma: no-cache")
             ->header("X-Content-Type-Options: nosniff")
             ->header("X-XSS-Protection: 1; mode=block")
             ->header("X-Frame-Options: SAMEORIGIN")
             ->output($page)
             ->buffering()
             ->display();        
    }          
}