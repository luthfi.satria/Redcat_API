<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Login_Controller extends FI_Controller
{
	
	function __construct()
	{
	    parent::__construct();
        $this->load->model(array('login_model'));
        $this->vars['_CACHE_DISABLED']  = true;
        $this->vars['_HOOK_EXPIRES']    = 0;	   
    }

    function loginview($page = ''){
        if($_SERVER['REMOTE_ADDR'] == '::1'){
            $profiler = FALSE;
        }
        else{
            $profiler = FALSE;
        }
        self::load_config('login');
        $this->vars['bahasa']           = $this->resources_model->translation($this->language);
        $this->vars['_META']            = '';        
        $this->vars['_PROFILER']        = $profiler;        
        $this->vars['_FLASH']           = self::flash();
        $this->vars['_INDEX_BODY']      = $page != '' ? $this->load->view($page, $this->vars, true) : '';
        self::master_page();
    }

    /* ------------------------------------------------------------------------------------------------------
    ** TEMPLATE INDEX
    ** ------------------------------------------------------------------------------------------------------
    */
    function master_page($hooking = true){

        if(!isset($this->vars['_TITLE'])){
            $this->vars['_TITLE'] = ucwords($this->vars['_INFO_TITLE']);
        }

        $page = $this->load->view('template/'.TEMPLATE.'/index',$this->vars,$hooking);
        
        if($hooking == true){
            self::outputs($page);    
        }
    }

    function outputs($page, $mime = 'text/html', $buffer = true){
        $this->myhook
             ->mime($mime)
             ->charset('utf-8')
             ->status(200)
             ->expires('-20 year')
             ->cache(0)
             ->header('Accept-Ranges: bytes')
             ->header('HTTP/1.0 200 OK')
             ->header('HTTP/1.1 200 OK')
             ->header('Cache-Control: no-cache, no-store, must-revalidate')
             ->header('Cache-Control: post-check=0, pre-check=0', false)
             ->header('Pragma: no-cache')
             ->header("X-Content-Type-Options: nosniff")
             ->header('X-XSS-Protection: 1; mode=block')
             ->header("X-Frame-Options: SAMEORIGIN")
             ->output($page)
             ->buffering()
             ->display();        
    }         	
}