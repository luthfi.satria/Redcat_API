<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Public_Controller extends FI_Controller
{

    function __construct()
    {
        parent:: __construct(); 
    }

    function invalid_response(){
        return $output = array(
                'code' => 500,
                'message'=> 'Validation error',
                'error' => $this->form_validation->error_array(),
            );
    }

    /* ------------------------------------------------------------------------------------------------------
    ** TEMPLATE INDEX
    ** ------------------------------------------------------------------------------------------------------
    */

    function outputs($page, $mime = 'text/html', $buffer = true){
        $this->myhook
             ->mime($mime)
             ->charset('utf-8')
             ->status(200)
             ->expires('-10 year')
             ->cache(0)
             ->header("Accept-Ranges: bytes")
             ->header("HTTP/1.0 200 OK")
             ->header("HTTP/1.1 200 OK")
             ->header("Cache-Control: no-cache, no-store, must-revalidate", false)
             ->header("Cache-Control: post-check=0, pre-check=0", false)
             ->header('Access-Control-Request-Headers: X-Requested-With, accept, content-type')
             ->header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, OPTIONS')
             // ->header('Access-Control-Allow-Origin: *')
             // ->header('Access-Control-Allow-Origin: http://vave.co.id')
             ->header('Access-Control-Allow-Headers: Api-Key')             
             ->header("Pragma: no-cache")
             ->header("X-Content-Type-Options: nosniff")
             ->header("X-XSS-Protection: 1; mode=block")
             ->header("X-Frame-Options: SAMEORIGIN")
             ->output($page)
             ->buffering()
             ->display();        
    }        
}