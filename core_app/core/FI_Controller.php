<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
// require_once('core_app/libraries/REST_Controller.php');
require_once(APPPATH.'libraries/REST_Controller.php');

use Restserver\Libraries\REST_Controller as REST_Controller;

class FI_Controller extends REST_Controller
{
    var $vars;
    var $template;
    var $cfg;
    var $css;
    var $js;
    var $datetime;

    function __construct()
    {
       parent::__construct();
    //    date_default_timezone_set('Asia/Jakarta');
       $this->load->library('predis_lib');
       $this->datetime = date('Y-m-d H:i:s');
       // $this->cfg = self::default_reference();     
    }

    function force_response($data, $http_code){
        $this->response($data, $http_code);
        $this->output
            //  ->set_header("Cache-Control: no-cache, no-store, must-revalidate", false)
            //  ->set_header("Cache-Control: post-check=0, pre-check=0", false)
            //  ->set_header('Access-Control-Request-Headers: X-Requested-With, accept, content-type')
            //  ->set_header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, OPTIONS')
            //  ->set_header('Access-Control-Allow-Origin: *')
            //  ->set_header('Access-Control-Allow-headers: Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Access-Control-Allow-Origin, Access-Control-Request-Headers, Authorization')
            //  ->set_header("Pragma: no-cache")
            //  ->set_header("X-Content-Type-Options: nosniff")
            //  ->set_header("X-XSS-Protection: 1; mode=block")
            //  ->set_header("X-Frame-Options: SAMEORIGIN")
             ->_display();
        exit;
    }

    function response_ok($data, $append = array(), $message = ''){
        $response = [
            'status' => true,
            'code' => SELF::HTTP_OK,
            'result' => $data,
            'message' => $message
        ];
        if(!empty($append) && is_array($append)){
            $response = array_merge($response, $append);
        }
        self::force_response($response, SELF::HTTP_OK);        
    }

    function response_failed($code, $message = 'Unauthorized', $append = array()){
        $response = [
            'status' => FALSE,
            'code'   => $code,
            'message' => $message
        ];

        if(!empty($append) && is_array($append)){
            $response = array_merge($response, $append);
        }
        self::force_response($response, $code);        
    }

    function create_log($module, $username = '', $message = ''){
        $datelog    = date('dmY');
        $dir = APPPATH."logs/".$module;
        $filepath = $dir.'/'.$datelog.'.txt';
        if(!is_dir($dir)){
            mkdir($dir, 0755, true);
        }

        $mockdata = [
            "datetime"  => date('d-m-Y H:i:s'),
            "ip_addr"   => $_SERVER["REMOTE_ADDR"],
            "username"  => $username,
            "message"   => $message,
            // "parameter" => "[".preg_replace("/(?:[\{|\r\n | \n])(?:[\s|\}])+/","$1",file_get_contents("php://input"))."]"
            "parameter" => preg_replace("/[\r\n|\n|\t]/","",file_get_contents("php://input"))
        ];

        // $mockdata = implode(" ", $mockdata)."\r\n";
        $mockdata = json_encode($mockdata)."\r\n\r\n";

        if(!write_file($filepath, $mockdata, 'a+')){
            self::response_failed(SELF::HTTP_INTERNAL_SERVER_ERROR, 'Unable to save logging data');
        }
    }    

    /* ------------------------------------------------------------------------------------------------------
    ** LOAD DEFAULT CONFIGURATION
    ** ------------------------------------------------------------------------------------------------------
    */
    function default_reference(){
        if($this->load->is_loaded('resources_model') == false){
            $this->load->model('resources_model');
        }
        $q   = $this->resources_model->get_config(array('all'));
        return $q;
    }

    function post_validation_response($postdata, $type = "validation error", $status = false){
        return $flash = array(
                'success' => $status, 
                'modal'   => true,
                'msg'     => self::set_flash('validation error', $postdata, false),
                'error'   => array_keys($this->form_validation->error_array()),
            );        
    }

    function api_invalid_response(){
        return $output = array(
                'code' => 500,
                'message'=> 'Validation error',
                'error' => $this->form_validation->error_array(),
            );
    }
}

foreach (glob(APPPATH."core/*.php") as $filename) {
    if($filename != 'FI_Controller'){
        include_once($filename);
    }
}
