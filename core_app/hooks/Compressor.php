<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Compressor
 {

	function minimize()
	{
		$CI =& get_instance();
		$buffer = $CI->output->get_output();
		$re = '%# Collapse whitespace everywhere but in blacklisted elements.
		        (?>             # Match all whitespans other than single space.
		          [^\S ]\s*     # Either one [\t\r\n\f\v] and zero or more ws,
		        | \s{2,}        # or two or more consecutive-any-whitespace.
		        ) # Note: The remaining regex consumes no text at all...
		        (?=             # Ensure we are not in a blacklist tag.
		          [^<]*+        # Either zero or more non-"<" {normal*}
		          (?:           # Begin {(special normal*)*} construct
		            <           # or a < starting a non-blacklist tag.
		            (?!/?(?:textarea|pre|script)\b)
		            [^<]*+      # more non-"<" {normal*}
		          )*+           # Finish "unrolling-the-loop"
		          (?:           # Begin alternation group.
		            <           # Either a blacklist start tag.
		            (?>textarea|pre|script)\b
		          | \z          # or end of file.
		          )             # End alternation group.
		        )  # If we made it here, we are not in a blacklist tag.
		        %Six';

		$newbuffer = preg_replace($re, " ", $buffer);

		if($newbuffer === null){
			$newbuffer = $buffer;
		}

		$mime = $CI->output->get_content_type($buffer);

		$CI->output
				  ->set_content_type($mime, 'utf-8')
				  // ->set_header('Accept-Ranges: bytes')
				  // ->set_header('HTTP/1.0 200 OK')
				  // ->set_header('HTTP/1.1 200 OK')
				  // ->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT')
				  // ->set_header("Expires: ".gmdate('D, d M Y H:i:s', strtotime('+3 hour'))." GMT")
				  // ->set_header('Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=14400')
				  // ->set_header('Cache-Control: post-check=0, pre-check=0')
				  // ->set_header('Pragma: no-cache')
				  ->set_header("X-Content-Type-Options: nosniff")
				  ->set_header('X-XSS-Protection: 1; mode=block')
				  ->set_header("X-Frame-Options: SAMEORIGIN")
				  ->cache(7200)
				  ->set_output($newbuffer)
				  ->_display();
		exit;
	}

 } 
 
/* End of file compress.php */
/* Location: ./system/application/hooks/compress.php */