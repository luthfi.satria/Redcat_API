<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* DISCLAIMER
* LIBRARY EXCELIO - EXCEL READER DRAWING
* COPYRIGHT : 2018 
* ORIGINAL AUTHOR : LUTHFI SATRIA RAMDHANI
* 1. DILARANG UNTUK MENGAMBIL ALIH HAK CIPTA PEMBUATAN TANPA PERSETUJUAN AUTHOR
* 2. DILARANG MENGHAPUS BARIS DISCLAIMER INI
* 3. DILARANG MENGKOMERSILKAN LIBRARY INI DALAM BENTUK APAPUN KEPADA SIAPAPUN TANPA PERSETUJUAN ATAU TANPA MEMBERIKAN PENGHARGAAN ATAU KONTRIBUSI ATAU DONASI KEPADA SAYA SEBAGAI PENULIS
* 4. ANDA DIPERSILAHKAN MENGUBAH ATAU MENAMBAHKAN ATAU MENYISIPKAN KODE DI DALAM LIBRARY INI DAN MENAMBAHKAN NAMA ANDA SEBAGAI AUTHOR TANPA MENGHILANGKAN NAMA AUTHOR SEBELUMNYA
* 5. ANDA PASTI TAHU DAN MERASAKAN SUSAH PAYAHNYA MEMBUAT SEBUAH KODE PROGRAM, JADI HARGAILAH SESAMA PROGRAMMER
* 6. SEGALA BENTUK KECURANGAN INSYAALLAH AKAN MENDAPATKAN BALASAN DI DUNIA MAUPUN AKHIRAT, TETAPLAH JUJUR KAWAN
*
* TERIMA KASIH
*
*
* SEGALA PERTANYAAN DAN DISKUSI DAPAT ANDA KIRIMKAN KE ALAMAT EMAIL DI BAWAH INI :
* luthfi_its@yahoo.com
*/
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Color;

require_once APPPATH.'../vendor/phpoffice/phpspreadsheet/src/Bootstrap.php';

class Excelspreadsheet
{
	protected $xlsheet;
	protected $sheet;
	protected $phpexcel;
	protected $objDrawing;
	protected $objReader;
	protected $reader;
	protected $filename		= 'excel_report';
	protected $setting 		= array();
	protected $excels 		= array();
	protected $nsheet		= array();
	protected $dkeys 		= array();
	protected $add_column 	= array();

	function construct(){
		ini_set("memory_limit", "256M");
		ini_set("max_execution_time", "300");

		$helper = new Sample();
		if ($helper->isCli()) {
		    $helper->log('This example should only be run from a Web Browser' . PHP_EOL);

		    return;
		}
		$this->setting = $params;

	}

	function init_configs($params = array()){
		$this->setting = $params;
		return $this;
	}

	/*
	* -----------------------------------------------------------------------------------------------
	* ###################### EXCEL DRAWING ( CONVERT TO EXCEL FILE) #################################
	* -----------------------------------------------------------------------------------------------
	*/

	/*
	* SET FILENAME
	* INISIALISASI NAMA FILE
	* @param string $string
	*/

	function set_filename($string = ''){
		$this->filename = $string;
		return $this;
	}

	/*
	* SET FIELD DATA
	* SET FIELD DATA YANG AKAN DITAMPILKAN
	* @param array $array
	*/

	function set_fields($array = array()){
		$this->dkeys = $array;
		return $this;
	}	

	/*
	* CREATE_SHEET
	* MENDEFINISKAN WORKSHEET YANG AKAN DIBUAT
	* param array / string $sheetname => [worksheet_name[,data]]
	* param array $data => data yang akan diretrieve di masing-masing worksheet
	*/
	function create_sheet($sheetname, $data = null){
		if(is_array($sheetname) && is_null($data)){
			// param array $sheetname => [[sheet => '',$data]]
			foreach ($sheetname as $key => $value) {
				self::create_sheet($value[0],$value[1]);
			}
		}
		else{
			// param array $sheet => [sheet [, data]]
			$this->nsheet[] = array('title'=>$sheetname, 'data'=>$data); 
		}
		return $this;
	}

	/*
	* new_sheet
	* Create new_sheet
	* param string $title => worksheet name
	*/
	function new_sheet($title = ''){
		$this->sheet = $this->xlsheet
							->createSheet()
							->setTitle($title)
							->setShowGridlines(false);
	}

	/*
	* rowdata
	* RETRIEVE EXCEL ROW CELL
	* param array / object $data
	*/
	private function rowdata($data = array()){
		$row = 2;
		$arraycols = array();

		if(isset($data)){
			foreach ($data as $key => $value) {
				$cols = 'A';
				if(empty($this->dkeys)){
					foreach ($value as $k => $v) {
						if($key == 0){
							$title = ucwords(str_replace('_', ' ', $k));
							$this->sheet
								 ->setCellValue($cols.($row-1),$title);	
						}
						$this->sheet
							 ->setCellValue($cols.$row, $v);
						array_push($arraycols, $cols);
						$cols++;						
					}
				}
				else{
					foreach ($this->dkeys as $k => $v) {
						if(array_key_exists($k, $value)){
							if($key == 0){
								$title = ucwords(str_replace('_', ' ', $v));
								$this->sheet
								 ->setCellValue($cols.($row-1),$title);
							}
							$this->sheet
							 	 ->setCellValue($cols.$row, $value[$k]);

							array_push($arraycols, $cols);
							$cols++;
						}
					}
				}
				$row++;
			}
		}
		foreach ($arraycols as $key => $value) {
			$this->sheet
				 ->getColumnDimension($value)
				 ->setAutoSize(true);
		}
		$this->sheet
			 ->getStyle('A1:'.end($arraycols).'1')
			 ->getFont()
			 ->setBold(true);

		$this->sheet
			 ->getStyle('A1:'.end($arraycols).($row - 1))
			 ->applyFromArray(self::excel_default_borders());

		$this->sheet->getStyle('A1:'.end($arraycols).'1')
			  ->getAlignment()
			  ->setVertical(Alignment::VERTICAL_CENTER);

		$this->sheet->getStyle('A1:'.end($arraycols).'1')
			  ->getAlignment()
			  ->setHorizontal(Alignment::HORIZONTAL_CENTER);

		$this->sheet
			 ->getPageSetup()
			 ->setRowsToRepeatAtTopByStartAndEnd(1,1);
		unset($arraycols);
	}

	/*
	| -----------------------------------------------------------------------------------------------------------------
	| EXCEL REPORT RENDERING
	| -----------------------------------------------------------------------------------------------------------------
	*/

	/*
	* PROPERTIES
	* Excel Spreadsheet Properties
	*/
	function properties(){
		$this->xlsheet = new Spreadsheet();		

		return $this->xlsheet
					->getProperties()
					->setCreator($this->setting['_INFO_OWNER'])
					->setLastModifiedBy($this->setting['_INFO_OWNER'])
					->setTitle("Office 2007 XLSX Test Document")
					->setSubject("Office 2007 XLSX Test Document")
					->setDescription("office report 2007, generated using PHP classes.")
					->setKeywords("office 2007 openxml php")
					->setCategory("Report file");
	}

	/*
	* body
	* CREATE WORKSHEET NAME
	* param array / object $data
	*/
	function body(){
		foreach ($this->nsheet as $key => $value) {
			self::new_sheet($value['title']);
			self::rowdata($value['data']);
		}
	}

	function rendered($save_as = 'Xlsx'){
		$objWriter = IOFactory::createWriter($this->xlsheet, $save_as);
		ob_end_clean();
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0		
		$objWriter->save('php://output');
		exit;
	}

	/*
	* generate
	* GENERATE EXCEL DRAWING
	*/
	function generate(){
		self::properties();
		self::body();
		self::styles();		
		self::rendered();
		$this->xlsheet->disconnectWorksheets();
		return $this;
	}

	/*
	* styles
	* INISIALISASI EXCEL STYLE
	* param array $param => [font="Arial" [,size = 10 [, fitpage = true [,fitwidth = true [, fitheight = false [, paper = PAPERSIZE_A4 [, orientation= ORIENTATION_PORTRAIT]]]]] ]
	*/
	function styles($param = null){
		$font = !empty($param) && array_key_exists('font', $param) ? $param['font'] : 'Arial';
		$size = !empty($param) && array_key_exists('size', $param) ? $param['size'] : 10;
		$orientation = !empty($param) && array_key_exists('orientation', $param) ? $param['orientation'] : (PageSetup::ORIENTATION_PORTRAIT);
		$paper 		= !empty($param) && array_key_exists('paper', $param) ? $param['paper'] : (PageSetup::PAPERSIZE_A4);
		$fitpage 	= !empty($param) && array_key_exists('fitpage', $param) ? $param['fitpage'] : true;
		$fitwidth 	= !empty($param) && array_key_exists('fitwidth', $param) ? $param['fitwidth'] : true;
		$fitheight 	= !empty($param) && array_key_exists('fitheight', $param) ? $param['fitheight'] : false;


		$this->xlsheet
			 ->getDefaultStyle()
			 ->getFont()
			 ->setName($font)
			 ->setSize($size);

		$this->sheet
			 ->getPageSetup()
			 ->setOrientation($orientation)
			 ->setPaperSize($paper)
			 ->setFitToPage($fitpage)
			 ->setFitToWidth($fitwidth)
			 ->setFitToHeight($fitheight);
		return $this;
	}

/*
| -------------------------------------------------------------------------------------------------------------------
| EXCEL REPORT STYLE
| -------------------------------------------------------------------------------------------------------------------
*/	
	function excel_default_borders(){
    	return $default= array(
                          'borders' => array(
                          'allborders' => array(
                                            'style' => Border::BORDER_THIN,
                                            'color' => array('argb' => '33000000')
                                          )
                                        )
                          );

	}

	function excel_outline_borders(){
    	return $outline = array(
                          'borders' => array(
                              'outline' => array(
                                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                                                'color' => array('argb' => '33000000')
                                            )
                                        )
                          );

	}


	/*
	* -----------------------------------------------------------------------------------------------
	* END OF EXCEL DRAWING
	* -----------------------------------------------------------------------------------------------	
	*/
}