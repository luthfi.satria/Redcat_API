<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Api_authenticate
{

	function digest_handler()
    {
    	$ci = &get_instance();
        $digest = isset($_SERVER['PHP_AUTH_DIGEST']) ? $_SERVER['PHP_AUTH_DIGEST'] : null;
        $nonce = uniqid();
               
        // If there was no digest, show login
        if (empty($digest))
        {
        	self::_forceLogin();
        } 
        
        preg_match_all('@(username|nonce|uri|nc|cnonce|qop|response)'.
                    '=[\'"]?([^\'",]+)@', $digest, $t);
        $digest_parts = array_combine($t[1], $t[2]); 
        
        $valid_logins = $ci->config->item('rest_valid_logins');
        $valid_pass = $valid_logins[$digest_parts['username']];
        
        // Based on all the info we gathered we can figure out what the response should be 
        $A1 = md5($digest_parts['username'] . ':' . $ci->config->item('rest_realm') . ':' . $valid_pass);
        $A2 = md5($_SERVER['REQUEST_METHOD'].':'.$digest_parts['uri']);
        
        $valid_response = md5($A1.':'.$digest_parts['nonce'].':'.$digest_parts['nc'].':'.$digest_parts['cnonce'].':'.$digest_parts['qop'].':'.$A2);
            
        if ($digest_parts['response'] != $valid_response)
        {
            self::_forceLogin($nonce);
        }

    }	

    private function _forceLogin(){
    	$ci = &get_instance();
            $ci->output
            	 ->set_content_type('application/json')
             	 ->set_header("Accept-Ranges: bytes")
             	 ->set_header("HTTP/1.0 401 Unauthorized")
             	 ->set_header("HTTP/1.1 401 Unauthorized")
             	 ->set_header("Cache-Control: no-cache, no-store, must-revalidate", false)
             	 ->set_header("Cache-Control: post-check=0, pre-check=0", false)
             	 ->set_header("Pragma: no-cache")
             	 ->set_header("X-Content-Type-Options: nosniff")
             	 ->set_header("X-XSS-Protection: 1; mode=block")
             	 ->set_header("X-Frame-Options: SAMEORIGIN")
             	 ->_display(json_encode(array(
             	 	'status' => false,
             	 	'code'	 => 400,
             	 	'message' => 'Unauthorized'
             	 )));
             exit();
    }
}