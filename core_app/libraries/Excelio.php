<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* DISCLAIMER
* LIBRARY EXCELIO - EXCEL READER DRAWING
* COPYRIGHT : 2016 
* ORIGINAL AUTHOR : LUTHFI SATRIA RAMDHANI
* 1. DILARANG UNTUK MENGAMBIL ALIH HAK CIPTA PEMBUATAN TANPA PERSETUJUAN AUTHOR
* 2. DILARANG MENGHAPUS BARIS DISCLAIMER INI
* 3. DILARANG MENGKOMERSILKAN LIBRARY INI DALAM BENTUK APAPUN KEPADA SIAPAPUN TANPA PERSETUJUAN ATAU TANPA MEMBERIKAN PENGHARGAAN ATAU KONTRIBUSI ATAU DONASI KEPADA SAYA SEBAGAI PENULIS
* 4. ANDA DIPERSILAHKAN MENGUBAH ATAU MENAMBAHKAN ATAU MENYISIPKAN KODE DI DALAM LIBRARY INI DAN MENAMBAHKAN NAMA ANDA SEBAGAI AUTHOR TANPA MENGHILANGKAN NAMA AUTHOR SEBELUMNYA
* 5. ANDA PASTI TAHU DAN MERASAKAN SUSAH PAYAHNYA MEMBUAT SEBUAH KODE PROGRAM, JADI HARGAILAH SESAMA PROGRAMMER
* 6. SEGALA BENTUK KECURANGAN INSYAALLAH AKAN MENDAPATKAN BALASAN DI DUNIA MAUPUN AKHIRAT, TETAPLAH JUJUR KAWAN
*
* TERIMA KASIH
*
*
* SEGALA PERTANYAAN DAN DISKUSI DAPAT ANDA KIRIMKAN KE ALAMAT EMAIL DI BAWAH INI :
* luthfi_its@yahoo.com
*/

class Excelio
{
	protected $sheet;
	protected $phpexcel;
	protected $objDrawing;
	protected $objReader;
	protected $reader;
	protected $setting;
	protected $filename		= 'excel_report';
	protected $excels 		= array();
	protected $nsheet		= array();
	protected $dkeys 		= array();
	protected $add_column 	= array();

	function __construct($params = null){
		require_once APPPATH.'third_party/phpexcel/Classes/PHPExcel.php';
		ini_set("memory_limit", "256M");
		ini_set("max_execution_time", "300");
		$this->setting 			= !empty($params) ? $params : array();
	}

/*
* CREATE OBJREADER
* FUNGSI INISIAL UNTUK MEMBACA FILE EXCEL
* param string $file_path => lokasi penyimpanan file excel yang akan dibaca beserta nama file dan ekstensinya
*/
	function create_objreader($file_path){
		$this->objReader = PHPExcel_IOFactory::load($file_path);
		return $this;
	}
/*
* activesheet
* INISIALISASI WORKSHEET YANG AKTIF YANG AKAN DIBACA
* param integer $index => index worksheet aktif
*/
	function activesheet($index){
		$this->reader 			= $this->objReader->setActiveSheetIndex($index);
		$this->highestColumn 	= $this->reader->getHighestColumn();
		return $this;
	}
/*
* row_index
* INISIALISASI BARIS EXCEL TABLE HEADER
* param integer $index => table header index
*/
	function row_index($index){
		$this->rowIndex = $index;
		return $this;
	}
/*
* start_index
* INISIALISASI BARIS PERTAMA YANG AKAN DI ITERATE
* param integer $index => baris tabel pertama yang akan di iterate
*/
	function start_index($index){
		$this->rowIterator = $this->reader->getRowIterator($index);
		return $this;
	}
/*
* start_column
* INISIALISASI KOLOM PERTAMA YANG AKAN DI ITERATE
* param integer $index => kolom tabel pertama yang akan di iterate
*/
	function start_column($index){
		$this->colstart = $index;
		return $this;
	}

	function change_array_keys(){
		$args = func_get_args();
		$this->dkeys = $args;
		return $this;
	}

	function add_column($column_name, $value){
		$this->add_column[$column_name] = $value;
		return $this; 
	}
/*
* read
* ITERASI PEMBACAAN FILE EXCEL
* Wajib ada
*/
	function read(){
		$result = array();
		$reader = $this->reader;
		foreach ($this->rowIterator as $key => $value) {
			$row = $value->getRowIndex();
			$i = 0;
			foreach (range($this->colstart, $this->highestColumn) as $k => $v) {
				if($reader->getCell($v.$row)->getValue() != ''){
					$dkey = !empty($this->dkeys) && array_key_exists($i, $this->dkeys) ? $this->dkeys[$i] : strtolower($reader->getCell($v.$this->rowIndex)->getValue());
					$result[$key][$dkey] = $reader->getCell($v.$row)->getValue();
					$i++;	
				}
			}

			if(!empty($this->add_column)){
				foreach ($this->add_column as $k => $v) {
					$result[$key][$k] = $v;
				}
			}
		}

		return array_values($result);
	}

/*
| -------------------------------------------------------------------------------------------------------------------
| EXCEL OBJECT DRAWING FUNCTION
| -------------------------------------------------------------------------------------------------------------------
*/

/*
* CREATE OBJDRAWING
* FUNGSI INISIAL UNTUK MENCETAK OUTPUT BERUPA FILE EXCEL
* FUNGSI INI WAJIB UNTUK DIINISIALISASIKAN
*/
	function create_objdrawing(){
		$this->objDrawing 		= new PHPExcel_Worksheet_Drawing();
		$this->phpexcel 		= new PHPExcel();
		return $this;
	}

/*
* FILENAME
* INISIALISASI NAMA FILE
* param string $string
*/
	function filename($string){
		$this->filename = $string;
		return $this; 
	}
/*
* CREATE_SHEET
* MENDEFINISKAN WORKSHEET YANG AKAN DIBUAT
* param array / string $sheetname => [worksheet_name[,data]]
* param array $data => data yang akan diretrieve di masing-masing worksheet
*/
	function create_sheet($sheetname, $data = null){
		if(is_array($sheetname) && is_null($data)){
			// param array $sheetname => [[sheet => '',$data]]
			foreach ($sheetname as $key => $value) {
				self::create_sheet($value[0],$value[1]);
			}
		}
		else{
			// param array $sheet => [sheet [, data]]
			$this->nsheet[] = array('title'=>$sheetname, 'data'=>$data); 
		}
		return $this;
	}	
/*
* excel_sheet
* INISIALISASI FILE EXCEL WRITER
* param string $sheetname => Nama worksheet
* param array $data => data yang akan diretrieve di sheet baru 
*/
	function excel_sheet($title = ''){
		$this->sheet = $this->phpexcel
							->createSheet()
							->setTitle($title)
							->setShowGridlines(false);
		return $this;
	}
/*
* excel_styles
* INISIALISASI EXCEL STYLE
* param array $param => [font="Arial" [,size = 10 [, fitpage = true [,fitwidth = true [, fitheight = false [, paper = PAPERSIZE_A4 [, orientation= ORIENTATION_PORTRAIT]]]]] ]
*/
	function excel_styles($param = null){
		$font = !empty($param) && array_key_exists('font', $param) ? $param['font'] : 'Arial';
		$size = !empty($param) && array_key_exists('size', $param) ? $param['size'] : 10;
		$orientation = !empty($param) && array_key_exists('orientation', $param) ? $param['orientation'] : PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT;
		$paper 		= !empty($param) && array_key_exists('paper', $param) ? $param['paper'] : PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4;
		$fitpage 	= !empty($param) && array_key_exists('fitpage', $param) ? $param['fitpage'] : true;
		$fitwidth 	= !empty($param) && array_key_exists('fitwidth', $param) ? $param['fitwidth'] : true;
		$fitheight 	= !empty($param) && array_key_exists('fitheight', $param) ? $param['fitheight'] : false;


		$this->sheet
			 ->getDefaultStyle()
			 ->getFont()
			 ->setName($font)
			 ->setSize($size);

		// $this->sheet
		// 	 ->getPageSetup()
		// 	 ->setOrientation($orientation)
		// 	 ->setPaperSize($paper)
		// 	 ->setFitToPage($fitpage)
		// 	 ->setFitToWidth($fitwidth)
		// 	 ->setFitToHeight($fitheight);
		return $this;
	}
/*
* remove_firstindex
* Menghapus worksheet pertama / worksheet default
*/
	function remove_firstindex(){
		$index = $this->sheet
			 		  ->getIndex($this->sheet->getSheetByName('Worksheet'));
		$this->sheet->removeSheetByIndex($index);
		return $this;
	}
/*
| -------------------------------------------------------------------------------------------------------------------
| EXCEL REPORT STYLE
| -------------------------------------------------------------------------------------------------------------------
*/	
	function excel_default_borders(){
    	return $default= array(
                          'borders' => array(
                          'allborders' => array(
                                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                                            'color' => array('argb' => '33000000')
                                          )
                                        )
                          );

	}

	function excel_outline_borders(){
    	return $outline = array(
                          'borders' => array(
                              'outline' => array(
                                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                                                'color' => array('argb' => '33000000')
                                            )
                                        )
                          );

	}

/*
| -------------------------------------------------------------------------------------------------------------------
| EXCEL REPORT DOCUMENT PROPERTIES
| -------------------------------------------------------------------------------------------------------------------
*/
	function excel_properties(){
		return $this->phpexcel->getProperties()
					->setCreator($this->setting['_INFO_OWNER'])
					->setLastModifiedBy($this->setting['_INFO_OWNER'])
					->setTitle("Office 2007 XLSX Test Document")
					->setSubject("Office 2007 XLSX Test Document")
					->setDescription("REPORT, generated using PHP classes.")
					->setKeywords("REPORT")
					->setCategory("Report file");

	}

/*
| -------------------------------------------------------------------------------------------------------------------
| EXCEL REPORT RENDER
| -------------------------------------------------------------------------------------------------------------------
*/	
	function rendered(){

		$objWriter = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel2007');
		ob_end_clean();
    	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$this->filename.'.xlsx"');
		header('Cache-Control: max-age=0');
		// header('Content-Type: application/vnd.ms-excel');
		// header('Content-Disposition: attachment;filename="'.$this->filename.'.xls"');
		$objWriter->save('php://output');
		exit;
	}

/*
* excel_rowdata
* RETRIEVE EXCEL ROW CELL
* param array / object $data
*/
	function excel_rowdata($data = array()){
		$row = 2;
		$arraycols = array();
		if(isset($data)){
			foreach ($data as $key => $value) {
				$cols = 'A';
				foreach ($value as $k => $v) {
					if($key == 0){
						$title = ucwords(str_replace('_', ' ', $k));
						$this->sheet
							 ->setCellValue($cols.($row-1),$title);	
					}
					$this->sheet
						 ->setCellValue($cols.$row, $v);
					array_push($arraycols, $cols);
					$cols++;						
				}
				$row++;
			}
		}
		foreach ($arraycols as $key => $value) {
			$this->sheet->getColumnDimension($value)->setAutoSize(true);
		}
		$this->sheet->getStyle('A1:'.end($arraycols).'1')->getFont()->setBold(true);
		$this->sheet->getStyle('A1:'.end($arraycols).($row - 1))->applyFromArray(self::excel_default_borders());
		$this->sheet->getStyle('A1:'.end($arraycols).'1')
			  ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->sheet->getStyle('A1:'.end($arraycols).'1')
			  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1,1);
		unset($arraycols);
	}

/*
* body
* CREATE WORKSHEET NAME
* param array / object $data
*/
	function body(){
		foreach ($this->nsheet as $key => $value) {
			self::excel_sheet($value['title']);
			self::excel_rowdata($value['data']);
		}
	}	
/*
* auto_gen
* GENERATE EXCEL DRAWING
*/
	function auto_gen(){
		self::excel_properties();
		self::body();
		self::excel_styles();
		unset($this->sheet);
		$this->phpexcel->removeSheetByIndex(0);
		self::rendered();
	}
}