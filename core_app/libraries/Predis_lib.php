<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* DISCLAIMER
* LIBRARY DBFORCE FOR BACKUP DATABASE
* COPYRIGHT : 2016 
* ORIGINAL AUTHOR : LUTHFI SATRIA RAMDHANI
* 1. DILARANG UNTUK MENGAMBIL ALIH HAK CIPTA PEMBUATAN TANPA PERSETUJUAN AUTHOR
* 2. DILARANG MENGHAPUS BARIS DISCLAIMER INI
* 3. DILARANG MENGKOMERSILKAN LIBRARY INI DALAM BENTUK APAPUN KEPADA SIAPAPUN TANPA PERSETUJUAN ATAU TANPA MEMBERIKAN PENGHARGAAN ATAU KONTRIBUSI ATAU DONASI KEPADA SAYA SEBAGAI PENULIS
* 4. ANDA DIPERSILAHKAN MENGUBAH ATAU MENAMBAHKAN ATAU MENYISIPKAN KODE DI DALAM LIBRARY INI DAN MENAMBAHKAN NAMA ANDA SEBAGAI AUTHOR TANPA MENGHILANGKAN NAMA AUTHOR SEBELUMNYA
* 5. ANDA PASTI TAHU DAN MERASAKAN SUSAH PAYAHNYA MEMBUAT SEBUAH KODE PROGRAM, JADI HARGAILAH SESAMA PROGRAMMER
* 6. SEGALA BENTUK KECURANGAN INSYAALLAH AKAN MENDAPATKAN BALASAN DI DUNIA MAUPUN AKHIRAT, TETAPLAH JUJUR KAWAN
*
* TERIMA KASIH
*
*
* SEGALA PERTANYAAN DAN DISKUSI DAPAT ANDA KIRIMKAN KE ALAMAT EMAIL DI BAWAH INI :
* luthfi_its@yahoo.com
*/

/**
 * 
 */
use Predis\Client;

class Predis_lib
{
	protected $redis;
	protected $conf;

	function __construct(){
		$CI = &get_instance();
       	$CI->config->load('redis', TRUE, TRUE);
        $this->conf = $CI->config->item('redis');
        $this->redis = new Client();
	}

    function ping(){
        try{
            $this->redis->connect();
            return $this->redis->ping(true);
        }
        catch(Predis\Connection\ConnectionException $e){
            return false;
        }
    }

    function get($command){
        return $this->redis->get($command);
    }

    function set($key, $val, $ttl = null){
        if($ttl != null){
            return $this->redis->setex($key, $ttl, $val);
        }
        return $this->redis->set($key, $val);
    }

    function del($key){
        return $this->redis->del($key);
    }
}