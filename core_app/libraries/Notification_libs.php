<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* DISCLAIMER
* LIBRARY EMAIL DAN SMS NOTIFICATION
* COPYRIGHT : 2018 
* ORIGINAL AUTHOR : LUTHFI SATRIA RAMDHANI
* 1. DILARANG UNTUK MENGAMBIL ALIH HAK CIPTA PEMBUATAN TANPA PERSETUJUAN AUTHOR
* 2. DILARANG MENGHAPUS BARIS DISCLAIMER INI
* 3. DILARANG MENGKOMERSILKAN LIBRARY INI DALAM BENTUK APAPUN KEPADA SIAPAPUN TANPA PERSETUJUAN ATAU TANPA MEMBERIKAN PENGHARGAAN ATAU KONTRIBUSI ATAU DONASI KEPADA SAYA SEBAGAI PENULIS
* 4. ANDA DIPERSILAHKAN MENGUBAH ATAU MENAMBAHKAN ATAU MENYISIPKAN KODE DI DALAM LIBRARY INI DAN MENAMBAHKAN NAMA ANDA SEBAGAI AUTHOR TANPA MENGHILANGKAN NAMA AUTHOR SEBELUMNYA
* 5. ANDA PASTI TAHU DAN MERASAKAN SUSAH PAYAHNYA MEMBUAT SEBUAH KODE PROGRAM, JADI HARGAILAH SESAMA PROGRAMMER
* 6. SEGALA BENTUK KECURANGAN INSYAALLAH AKAN MENDAPATKAN BALASAN DI DUNIA MAUPUN AKHIRAT, TETAPLAH JUJUR KAWAN
*
* TERIMA KASIH
*
*
* SEGALA PERTANYAAN DAN DISKUSI DAPAT ANDA KIRIMKAN KE ALAMAT EMAIL DI BAWAH INI :
* luthfi_its@yahoo.com
*/
class Notification_libs extends CI_Model
{
	var $result = array();

	function __construct(){
		parent::__construct();
	}

	function load_configs($ckey = 'cgid', $field = null){
		if(is_array($ckey)){
			foreach ($ckey as $key => $value) {
				if(is_array($value)){
					$this->db->where_in($key, $value);
				}
				else{
					$this->db->where($key, $value);
				}
			}
		}
		else{
			if(is_array($field)){
				$this->db->where_in($ckey, $field);
			}
			else{
				$this->db->where($ckey, $field);
			}
		}
		$sql = $this->db
					->select('idconfig,cgid,configname,configvalue')
					->from('configuration')
					->get()
					->result_array();
		if(!empty($sql)){
			foreach ($sql as $key => $value) {
				$this->result['config'][$value['cgid']][$value['configname']] = $value['configvalue'];
			}
		}
		$sql = '';
		return $this;
	}

	function set_apikey($name, $config_field){
		$this->result['ayosms'][$name] = $this->result['config'][5][$config_field];
		return $this;
	}

	function set_sender($sender, $urlencode = false){
		$this->result['ayosms']['from'] = $urlencode == true ? urlencode($sender) : $sender;
		return $this;
	}

	function set_receiver($receiver, $international_code = false, $kode_negara = '62'){
		if(is_array($receiver)){
			if($international_code == true){
				foreach ($receiver as $key => $value) {
					if(!preg_match('/[^+0-9]/',trim($value))){
						$first_char = substr(trim($value), 0, 1);
						if( $first_char == '+'){
				            $receiver[$key] = str_replace('+', '', trim($value));
				        }			        
					}
				}
			}

			$receiver = implode(',', $receiver);
		}
		else{
			if(!preg_match('/[^+0-9]/',trim($receiver))){
				$first_char = substr(trim($receiver), 0, 1);
				if( $first_char == '+'){          
					$receiver = str_replace('+', '', trim($receiver));
				}	
			}
		}
		$this->result['ayosms']['to'] = $receiver;
		return $this;
	}

	function set_message($name, $search = array(), $replace = array()){
		if(!empty($search) && !empty($replace)){
			$this->result['ayosms']['msg'] = str_replace($search, $replace, $this->result['config'][6][$name]);
		}
		else{
			$this->result['ayosms']['msg'] = $name;
		}
		return $this;
	}

	function set_message_id($unik = null){
		$this->result['ayosms']['trx_id'] = !empty($unik) ? 'SMS'.$unik : 'SMS'.strtotime('now');
		return $this;
	}

	function set_dlr($dlr = false){
		$this->result['ayosms']['dlr'] = $dlr;
		return $this;
	}

	function set_delivery_time($datetime){
		$this->result['ayosms']['delivery_time'] = date('YmdHi', strtotime($datetime));
		return $this;
	}

	function create_query_string(){
		$query = http_build_query($this->result['ayosms']);
		unset($this->result['ayosms']);
		$this->result['ayosms'] = 'https://api.ayosms.com/mconnect/gw/sendsms.php?'.$query;
		return $this;
	}

	function send_sms($method = 'GET'){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->result['ayosms']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		$this->result['output'] = curl_exec($ch);
		curl_close($ch);
		unset($this->result['ayosms']);
		return $this;		
	}

	/*
	* EMAIL NOTIFICATION
	*/	

	function set_email_sender($sender){
		$this->result['email']['sender'] = $sender;
		return $this;
	}

	function set_email_receiver($receiver_email){
		$this->result['email']['receiver'] = $receiver_email;
		return $this;
	}

	function set_email_cc($receiver_cc){
		$this->result['email']['cc'] = $receiver_cc;
		return $this;
	}

	function set_email_bcc($receiver_bcc){
		$this->result['email']['bcc'] = $receiver_bcc;
		return $this;
	}

	function set_email_subject($subject){
		$this->result['email']['subject'] = $subject;
		return $this;
	}

	function set_email_message($name, $search = array(), $replace = array()){
		if(!empty($search) && !empty($replace)){
			$this->result['email']['message'] = str_replace($search, $replace, $this->result['config'][6][$name]);
		}
		else{
			$this->result['email']['message'] = $name;
		}
		return $this;
	}

	function sending_email(){
		$this->load->library('email');
		$config = $this->result['config'][3];
		$maildata = $this->result['email'];
		$config['crlf'] 	= stripcslashes($config['crlf']);
		$config['newline'] 	= stripcslashes($config['newline']);
		$this->email->initialize($config);

		if(array_key_exists('cc', $maildata)){
			$this->email->cc($maildata['cc']);
		}
		if(array_key_exists('bcc', $maildata)){
			$this->email->cc($maildata['bcc']);
		}		
		$this->email
			 ->from($config['smtp_user'], $maildata['sender'])
			 ->to($maildata['receiver'])
			 ->subject($maildata['subject'])
			 ->message($maildata['message']);
		if($this->email->send()){
			unset($this->result['email']);
			return true;
		}
		else{
			show_error($this->email->print_debugger());
		}
		// return $this;
	}	
}