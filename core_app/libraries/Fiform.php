<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* DISCLAIMER
* LIBRARY FIFORM FOR CREATING FORM WITH BOOTSTRAP BASE
* COPYRIGHT : 2016 
* ORIGINAL AUTHOR : LUTHFI SATRIA RAMDHANI
* 1. DILARANG UNTUK MENGAMBIL ALIH HAK CIPTA PEMBUATAN TANPA PERSETUJUAN AUTHOR
* 2. DILARANG MENGHAPUS BARIS DISCLAIMER INI
* 3. DILARANG MENGKOMERSILKAN LIBRARY INI DALAM BENTUK APAPUN KEPADA SIAPAPUN TANPA PERSETUJUAN ATAU TANPA MEMBERIKAN PENGHARGAAN ATAU KONTRIBUSI ATAU DONASI KEPADA SAYA SEBAGAI PENULIS
* 4. ANDA DIPERSILAHKAN MENGUBAH ATAU MENAMBAHKAN ATAU MENYISIPKAN KODE DI DALAM LIBRARY INI DAN MENAMBAHKAN NAMA ANDA SEBAGAI AUTHOR TANPA MENGHILANGKAN NAMA AUTHOR SEBELUMNYA
* 5. ANDA PASTI TAHU DAN MERASAKAN SUSAH PAYAHNYA MEMBUAT SEBUAH KODE PROGRAM, JADI HARGAILAH SESAMA PROGRAMMER
* 6. SEGALA BENTUK KECURANGAN INSYAALLAH AKAN MENDAPATKAN BALASAN DI DUNIA MAUPUN AKHIRAT, TETAPLAH JUJUR KAWAN
*
* TERIMA KASIH
*
*
* SEGALA PERTANYAAN DAN DISKUSI DAPAT ANDA KIRIMKAN KE ALAMAT EMAIL DI BAWAH INI :
* luthfi_its@yahoo.com
*/
class Fiform
{
	protected $form;
	protected $required;
	protected $hidden;
	protected $field;
	protected $button;
	protected $index = 0;
	protected $trees;
	protected $key;
	protected $label;
	protected $sess;
	protected $addon;
	protected $error_inline = false;

	function __construct(){
		$ci = &get_instance();
		$session = $ci->session->flashdata('error');
		$this->sess['cache'] = !empty($session['cache']) ? $session['cache'] : array();
		$this->sess['error'] = !empty($session['array']) ? $session['array'] : array();
	}
/*
* CREATE
* FUNGSI INISIAL UNTUK MEMBUAT FORM
* param string $action => form action
* param array $attribute => form attribute
* param array $hidden => [name => value]
*/
	function create($action = '', $attribute = array(), $hidden = array() ){
		$this->form = form_open($action, $attribute, $hidden);
		return $this;		
	}
/*
* HIDDEN
* SET INPUT TYPE HIDDEN
* param string $param => nama input hidden / array $param => [name => value [, name => value]]
* param string $value
*/
	function hidden($param, $value = null){
		if(!is_array($param)){
			$this->hidden[$param] = $value;
		}
		else{
			array_filter($param, function($items, $key){
				$this->hidden[$key] = $items;
			}, ARRAY_FILTER_USE_BOTH);
		}
		return $this;
	}
/*
* REQUIRED
* SET INPUT YANG WAJIB TERISI (BERHUBUNGAN DENGAN VALIDASI FORM)
* format [input1[,input2[,input3]]]
*/
	function required(){
		$args = func_get_args();
		if(!empty($args)){
			array_map(function($items){
				$this->required[] = $items;
			}, $args);			
		}
		else{
			show_error('Anda belum memberikan parameter field yg wajib terisi pada fungsi required, contoh param1, param2, etc');
		}
		return $this;
	}
/*
* FIELD
* SET FIELD 
* format [type, label, param = [name[ ,id [,class [, etc]] ] ] ]
*/
	function field(){
		$args = func_get_args();
		if(!empty($args)){
			$this->field[$this->index][] = $args;	
		}
		else{
			show_error('function field memerlukan parameter [string $jenis_input, string $label, [$parameter_input]]');			
		}
		return $this;
	}
/*
* FIELDSET
* MEMBUAT FIELDSET GROUP
* param string $legend => nama legend
*/	
	function fieldset($legend = ''){
		$this->fieldset[$this->index] = '<fieldset>';
			$this->fieldset[$this->index] .= '<legend>'.$legend.'</legend>';
		return $this;
	}
/*
* ENDFIELDSET
* MENUTUP GROUP FIELDSET
*/
	function endfieldset(){
		$this->endfieldset[$this->index] = '</fieldset>';
		$this->index++;
		return $this;
	}

/*
* CAROUSEL
* MEMBUAT CAROUSEL GROUP
* param string $legend => nama legend
*/	
	function carousel($title = ''){
		$show = empty($this->carousel) ? ' in' : '';
		$collapse = empty($this->carousel) ? '' : ' collapsed';
		$this->carousel[$this->index] = empty($this->carousel) ? '<div class="panel-group form-accor" role="tablist" aria-multiselectable="false">' : '';
		$this->carousel[$this->index] = '<div class="panel panel-default">';
			$this->carousel[$this->index] .= '<div class="panel-heading" role="tab" id="fcarhead_'.$this->index.'">';
				$this->carousel[$this->index] .= '<h4 class="panel-title">';
					$this->carousel[$this->index] .= '<a data-toggle="collapse" data-parent=".form-accor" href="#fcarcollapse_'.$this->index.'" aria-expanded="true" aria-controls="fcarcollapse_'.$this->index.'" title="Click to hide or show" class="'.$collapse.'">';
					$this->carousel[$this->index] .= $title;
					$this->carousel[$this->index] .= ' <i class="arrow"></i>';
					$this->carousel[$this->index] .= '</a>';
				$this->carousel[$this->index] .= '</h4>';
			$this->carousel[$this->index] .= '</div>';
			$this->carousel[$this->index] .= '<div id="fcarcollapse_'.$this->index.'" class="panel-collapse collapse'.$show.'" role="tabpanel" aria-labelledby="fcarhead_'.$this->index.'">';
				$this->carousel[$this->index] .= '<div class="panel-body">';
		return $this;
	}

/*
* END CAROUSEL
* MENUTUP GROUP CAROUSEL
*/
	function endcarousel(){
		$this->endcarousel[$this->index] = '</div></div></div>';
		$this->index++;
		return $this;
	}

/*	
/*
* NEW ROW
* MEMBUAT BARIS BARU PADA FORM
*/
	function new_row(){
		$this->index++;
		return $this;
	}
/*
* BTN
* MEMBUAT FORM BUTTON
* param string $title
* param string $attribute
* param string $icon
*/
	function btn($title = '', $attribute = '', $icon = ''){
		if(is_array($attribute)){
			// $attribute 	= implode(' ', array_filter($attribute, function(&$items, $key){
			// 	$items = $key.'="'.$items.'"';
			// 	return $items;
			// }, ARRAY_FILTER_USE_BOTH));	
			$attr = '';
			foreach ($attribute as $key => $value) {
				$attr .= $key.'="'.$value.'" ';
			}
		}
		$attr .= ' title="'.ucfirst($title).'"';
		if($attribute['type'] == 'button' || $attribute['type'] == 'submit'){
			$this->button[] = '<button '.$attr.'>'.$icon.' '.ucfirst($title).'</button>';
		}
		else{
			$this->button[] = '<a '.$attr.'>'.$icon.' '.ucfirst($title).'</a>';
		}
		return $this;
	}
/*
* GENERATE FIELD
* MENGEKSEKUSI FIELD
*/
	function error_inline(){
		$this->error_inline = true;
		return $this;
	}

	private function generate_field($field, $sb = ''){
		if(!empty($field)){
			array_walk($field, function($items, $key) use(&$sb){
				$cols 	= 12 / count($items);
				$sb 	.= !empty($this->carousel) && array_key_exists($key, $this->carousel) == true ? $this->carousel[$key] : '';
				$sb 	.= !empty($this->fieldset) && array_key_exists($key, $this->fieldset) == true ? $this->fieldset[$key] : '';
				$sb 	.= '<div class="form-group">';
				array_walk($items, function($value) use($cols, &$sb){
					$sb .= '<div class="col-xs-12 col-md-'.$cols.'">';
						if(!empty($value[1])){
							$sb .= '<label>'.ucwords($value[1]).'</label>';	
						}
						$required = !empty($value[2]) && is_array($value[2]) && array_key_exists('name', $value[2]) && !empty($this->required) ? in_array($value[2]['name'], $this->required) : false;
						$required = $required == true ? ' required' : '';

						$required .= !empty($required) && array_key_exists($value[2]['name'], $this->sess['error']) ? ' has-error' : '';
						$sb .= '<span class="'.$required.'">';
							$func = $value[0];
							$sb .= self::$func($value[2]);
							if($func == 'filesimage'){
								$val = array_key_exists('value', $value[2]) && !empty($value[2]) ? $value[2]['value'] : null;
	                       		if(!empty($value)){
	                       			unset($value[2]['value']);
	                       		}
	                       		// $class = array_key_exists('class', $value[2]) ? $value[2]['class'] : null;
	                       		// echo json_encode($value[2]);
	                       		$sb .= self::pre_loaded_image($val, $value[2]);
							}
						$sb .= '</span>';

						if(array_key_exists(3, $value) == true){
							$sb .= '<small>'.$value[3].'</small>';
						}

						$sb .= $this->error_inline == true && !empty($required) && array_key_exists($value[2]['name'], $this->sess['error']) ? '<div class="error-message">'.$this->sess['error'][$value[2]['name']].'</div>' : ''; 
						// $sb .= '<div class="input-group'.$required.'">';
						// $sb .= '</div>';
					$sb .= '</div>';
				});
				$sb 	.= '</div>';
				$sb 	.= !empty($this->endfieldset) && array_key_exists($key, $this->endfieldset) == true ? $this->endfieldset[$key] : '';
				$sb 	.= !empty($this->endcarousel) && array_key_exists($key, $this->endcarousel) == true ? $this->endcarousel[$key] : '';
			});
			unset($field);
		}
		return $sb;
	}

	function form_addon($html = ''){
		$this->addon = $html;
		return $this;
	}
/*
* RETRIEVE
* HASIL AKHIR FORM
*/
	function retrieve(){
		$sb 	= !empty($this->form) ? $this->form : '';
		// $sb 	.= '<div class="form-container">';
		$sb 	.= !empty($this->hidden) ? form_hidden($this->hidden) : '';
		$sb 	.= !empty($this->carousel) ? '<div class="panel-group form-accor" role="tablist" aria-multiselectable="false">' : '';
		$sb 	.= self::generate_field($this->field);
		$sb 	.= !empty($this->carousel) ? '</div>' : '';
		unset($this->carousel);
		$sb 	.= !empty($this->addon) ? $this->addon : '';
		unset($this->addon);
		// $sb 	.= '</div>';
		$sb 	.= '<div class="form-group">';
		$sb 	.= '<div class="col-xs-12 text-right">';
		$sb 	.= implode(' ', $this->button);
		$sb 	.= '</div>';
		$sb 	.= '</div>';
		$sb 	.= !empty($this->form) ? form_close() : '';
		unset($this->field, $this->form, $this->button, $this->required);
		return $sb;
	}

/*
* -----------------------------------------------------------------------------------------------------------------
* FORM INPUT
* -----------------------------------------------------------------------------------------------------------------
*/

/*
* TEXT
* MEMBUAT INPUT TYPE TEXT
* param array $data => [name => string [, placeholder => string [,value => string [,class => string [,prefix => string [,suffix => string]  ]] ] ] ]
*/
	function text($data){
		$text 	= '';
		$text 	.= array_key_exists('prefix', $data) ? '<span class="input-group-addon strong">'.$data['prefix'].'</span>' : '';
		$suffix  = array_key_exists('suffix', $data) ? '<span class="input-group-addon strong">'.$data['suffix'].'</span>' : '';
		$suffix_btn = array_key_exists('suffix_btn', $data) ? '<span class="input-group-btn custom-group-btn">'.$data['suffix_btn'].'</span>': '';
		$data['class'] 	= array_key_exists('class', $data) ? $data['class'].' form-control' : 'form-control';

		if(array_key_exists('readonly', $data) && $data['readonly'] == true){
			$data['readonly'] = 'readonly';
		}
		else{
			unset($data['readonly']);
		}

		unset($data['prefix'], $data['suffix'], $data['suffix_btn']);

		$data['value'] = !empty($this->sess['cache']) && array_key_exists('name', $data) ? $this->sess['cache'][$data['name']] : (array_key_exists('value', $data) ? $data['value'] : '');
		$text .= form_input($data);
		$text .= $suffix_btn.$suffix;
		$data = $prefix = '';
		return $text;
	}
/*
* TEXT
* MEMBUAT INPUT TYPE NUMBER
* param array $data => [name => string [, placeholder => string [,value => string [,class => string [,prefix => string [,suffix => string]  ]] ] ] ]
*/
	function number($data){
		$text = '';
		$text 	.= array_key_exists('prefix', $data) ? '<span class="input-group-addon strong">'.$data['prefix'].'</span>' : '';
		$suffix  = array_key_exists('suffix', $data) ? '<span class="input-group-addon strong">'.$data['suffix'].'</span>' : '';

		$data['class'] 	= array_key_exists('class', $data) ? $data['class'].' form-control' : 'form-control';
		$data['value'] = !empty($this->sess['cache']) && array_key_exists('name', $data) ? $this->sess['cache'][$data['name']] : (array_key_exists('value', $data) ? $data['value'] : '');	
		unset($data['prefix'], $data['suffix']);

		$text .= '<input type="number" ';
		array_filter($data, function($items, $key) use(&$text){
			$text .= $key.'="'.$items.'"';
		}, ARRAY_FILTER_USE_BOTH);
		$text .= '/>';		
		$text .= $suffix;
		$data = $prefix = '';
		return $text;
	}
/*
* TEXT
* MEMBUAT INPUT TYPE PASSWORD
* param array $data => [name => string [, placeholder => string [,value => string [,class => string [,prefix => string [,suffix => string]  ]] ] ] ]
*/
	function password($data){
		$text = '';
		$text 	.= array_key_exists('prefix', $data) ? '<span class="input-group-addon strong">'.$data['prefix'].'</span>' : '';
		$suffix  = array_key_exists('suffix', $data) ? '<span class="input-group-addon strong">'.$data['suffix'].'</span>' : '';

		$data['class'] 	= array_key_exists('class', $data) ? $data['class'].' form-control' : 'form-control';
		unset($data['prefix'], $data['suffix']);

		$text .= '<input type="password" ';
		array_filter($data, function($items, $key) use(&$text){
			$text .= $key.'="'.$items.'"';
		}, ARRAY_FILTER_USE_BOTH);
		$text .= '/>';		
		$text .= $suffix;
		$data = $prefix = '';
		return $text;
	}
/*
* TEXT
* MEMBUAT INPUT SELECT
* param array $data => [name => string [, option => array, option_value => string, option_label => string [,value => string [,class => string [,prefix => string [,suffix => string ]  ]] ] ] ]
*/
	function dropdown($data){
		$text = '';
		$text 	.= array_key_exists('prefix', $data) ? '<span class="input-group-addon strong">'.$data['prefix'].'</span>' : '';
		$suffix  = array_key_exists('suffix', $data) ? '<span class="input-group-addon strong">'.$data['suffix'].'</span>' : '';

		$data['class'] 	= array_key_exists('class', $data) ? $data['class'].' form-control' : 'form-control';
		
		if(!empty($this->sess['cache']) && array_key_exists('name', $data) && array_key_exists(preg_replace('/(\[.*\])$/', '', $data['name']), $this->sess['cache'])){
			$val = $this->sess['cache'][preg_replace('/(\[.*\])/', '', $data['name'])];
			if(!is_array($val)){
				$this->value = [$val];
			}
		}
		elseif(array_key_exists('value', $data)){
			// if(is_string($data['value']) || is_bool($data['value'])){
			if(is_array($data['value'])){
				$this->value = $data['value'];
			}
			else{
				$this->value = [$data['value']];
			}
		}

		$opt_value 	 = array_key_exists('option_value', $data) ? $data['option_value'] : null;
		$opt_label 	 = array_key_exists('option_label', $data) ? $data['option_label'] : null;	
		unset($data['prefix'], $data['suffix'], $data['value'], $data['option_value'], $data['option_label']);
		
		$attribute = ' ';
		array_walk($data, function($items, $key) use(&$attribute){
			if($key != 'option'){
				$attribute .= $key.'="'.$items.'"';	
			}
		});

		if(array_key_exists('option', $data)){
			$text .= '<select'.$attribute.'>';
			array_filter($data['option'], function($items, $key) use($opt_value, $opt_label, &$text){
				if(is_object($items)){
					$items  = (array)$items;				
				}
				$key 		= !empty($opt_value) && array_key_exists($opt_value, $items) ? $items[$opt_value] : $key;
				$items		= !empty($opt_label) && array_key_exists($opt_label, $items) ? $items[$opt_label] : $items;
				$selected 	= !empty($this->value) && in_array($key, $this->value) == true ? ' selected' : '';
				$text .= '<option value="'.$key.'"'.$selected.'>';
					$text .= $items;
				$text .= '</option>';
			}, ARRAY_FILTER_USE_BOTH);
			$text .= '</select>';
		}
		$text .= $suffix;
		$this->value = $data = $suffix = $opt_value = $opt_label = '';
		return $text;
	}
/*
* TEXT
* MEMBUAT INPUT SELECT TREES
* param array $data => [name => string [, option => array, opt-index => string, opt-label => string, opt-group => string, opt-start => integer [,value => string [,class => string [,prefix => string [,suffix => string ]  ]] ] ] ]
*/
	function trees($data){
		$start = $data['opt-start']	= array_key_exists('opt-start', $data) ? $data['opt-start'] : 0;
		// $this->trees['exclude']		= array_key_exists('opt-exclude', $data) && !is_array($data['opt-exclude']) ? array($data['opt-exclude']) : (is_array($data['opt-exclude']) ? $data['opt-exclude'] : null);
		$this->trees['exclude'] 	= array_key_exists('opt-exclude', $data) ? (is_array($data['opt-exclude']) ? $data['opt-exclude'] : (array) $data['opt-exclude'] ) : null;
		$this->trees['index'] 		= $data['opt-index'];
		$this->trees['label']		= $data['opt-label'];
		$this->trees['group']		= $data['opt-group'];
		$this->trees['name']		= $data['name'];
		$this->trees['selected'] 	= $data['selected'];
		$this->trees['min']			= !empty($data['option']) ? min(array_map(function($items){
			$items = (array)$items; 
			return $items[$this->trees['index']];
		}, $data['option'])) : 0;
		$option 					= $data['option'];

		$text 	= '';
		$text 	.= array_key_exists('prefix', $data) ? '<span class="input-group-addon strong">'.$data['prefix'].'</span>' : '';
		$suffix  = array_key_exists('suffix', $data) ? '<span class="input-group-addon strong">'.$data['suffix'].'</span>' : '';
		unset($data['opt-index'], $data['opt-label'], $data['opt-group'], $data['name'], $data['selected'], $data['opt-start'], $data['option']);

		$attribute = '';
		if(!empty($data)){
			array_filter($data, function($items, $key) use(&$attribute){
				if($key != 'class'){
					$attribute .= ' '.$key.'="'.$items.'"';				
				}
			}, ARRAY_FILTER_USE_BOTH);			
		}
		$class = array_key_exists('class', $data) ? ' '.$data['class'] : '';
		$text .= '<div class="drop-tree-wrapper">'; 
		$text .= '<div class="drop-tree'.$class.'"'.$attribute.'>';
			$text .= '<ul class="drop-tree-option">';
				$text .= self::trees_option($option, $start);
			$text .= '</ul>';
		$text .= '</div>';
		$text .= $suffix;
		$text .= '<span class="input-group-btn drop-tree-btn">';
			$text .= '<button type="button" class="btn btn-default btn-drop btn-md"><i class="fa fa-caret-down fa-lg"></i></button>';
		$text .= '</span>';
		$text .= '</div>';

		unset($start, $data, $suffix, $attribute, $option);
		return $text;		
	}

	private function trees_option($data, $parent = 0){
		$text = '';
		array_walk($data, function($items) use($data, $parent, &$text){
			$items = (array) $items;
			if($items[$this->trees['group']] == $parent){
				if(!empty($this->trees['selected']) && $this->trees['selected'] == $items[$this->trees['index']]){
					$selected = ' checked="checked"';
				}
				elseif(empty($this->trees['selected']) && $this->trees['min'] == $items[$this->trees['index']]){
					$selected = ' checked="checked"';
				}
				else{
					$selected = '';
				}
				$readonly = !empty($this->trees['exclude']) && in_array($items[$this->trees['index']], $this->trees['exclude']) == true ? ' disabled' : '';
				$child = self::trees_option($data, $items[$this->trees['index']]);
				$vis 	  = !empty($selected) ? 'visible' : 'hide';
				$text .= '<li class="option-item">';
					$text .= '<div class="radio '.$vis.'">';
						$text .= '<label>';
							$text .= '<input type="radio" name="'.$this->trees['name'].'" value="'.$items[$this->trees['index']].'"'.$selected.$readonly.' />';
							$text .= empty($child) ? '<i class="fa fa-folder-o"></i>' : '<i class="fa fa-folder-open-o"></i>';
							$text .= ' ';
							$text .= ucwords($items[$this->trees['label']]);
						$text .= '</label>';
					$text .= '</div>';
					if(!empty($child)){
						$text .= '<ul>';
							$text .= $child;
						$text .= '</ul>';
					}
				$text .= '</li>';			
			}
		});
		unset($data);
		return $text;
	}
/*
* TEXT
* MEMBUAT INPUT TYPE RADIO
* param array $data => [name => string [, option => array, [,value => string [,class => string [,prefix => string [,suffix => string [, inline => bool] ]  ]] ] ] ]
*/
	function radio($data){
		$text = '';

		$text .= array_key_exists('prefix', $data) ? '<span class="input-group-addon strong">'.$data['prefix'].'</span>' : '';

		$data['class'] 	= array_key_exists('class', $data) ? $data['class'].' radio-control' : 'radio-control';


		if(!empty($this->sess['cache']) && array_key_exists(preg_replace('/(\[.*\])/', '', $data['name']), $this->sess['cache']) == true){
			$value = (array)$this->sess['cache'][preg_replace('/(\[.*\])/', '', $data['name'])];
		}
		else{
			$value = array_key_exists('value', $data) && empty($this->sess['cache']) ? (array)$data['value'] : array();
		}
		$inline = array_key_exists('inline', $data) && $data['inline'] == true ? ' radio-inline' : '';
		if(!empty($inline)){
			unset($data['inline']);
		}
		$attribute = '';
		array_walk($data, function($items, $key) use(&$attribute){
			if($key != 'option' && $key != 'value'){
				$attribute .= ' '.$key.'="'.$items.'"';	
			}
		});

		$text .= '<div class="radio-group">';
		array_filter($data['option'], function($items, $key) use(&$text, $attribute, $value, $inline){
			$checked = in_array($key, $value) ? ' checked="checked"' :'' ;
			$text .= '<div class="radio'.$inline.'">';
				$text .= '<label>';
					$text .= '<input type="radio"'.$attribute.' value="'.$key.'" '.$checked.'>';
					$text .= ucfirst($items);
				$text .= '</label>';
			$text .= '</div>';
		}, ARRAY_FILTER_USE_BOTH);
		$text .= '</div>';
		$text .= array_key_exists('suffix', $data) ? '<span class="input-group-addon strong">'.$data['suffix'].'</span>' : '';
		unset($data, $attribute);
		return $text;
	}
/*
* TEXT
* MEMBUAT INPUT SELECT
* param array $data => [name => string [, option => array, [,value => string [,class => string [,prefix => string [,suffix => string [,inline => bool ] ]  ] ] ] ] ]
*/
	function checkbox($data){
		$text = '';

		$text .= array_key_exists('prefix', $data) ? '<span class="input-group-addon strong">'.$data['prefix'].'</span>' : '';

		$data['class'] 	= array_key_exists('class', $data) ? $data['class'].' radio-control' : 'radio-control';


		if(!empty($this->sess['cache']) && array_key_exists(preg_replace('/(\[.*\])/', '', $data['name']), $this->sess['cache']) == true){
			$value = (array)$this->sess['cache'][preg_replace('/(\[.*\])/', '', $data['name'])];
		}
		else{
			$value = array_key_exists('value', $data) && empty($this->sess['cache']) ? (array)$data['value'] : array();
		}

		$inline = array_key_exists('inline', $data) && $data['inline'] == true ? ' checkbox-inline' : '';

		$attribute = '';
		unset($data['value']);
		array_walk($data, function($items, $key) use(&$attribute){
			if($key != 'option' && $key != 'inline'){
				$attribute .= ' '.$key.'="'.$items.'"';	
			}
		});

		$text .= '<div class="checkbox-group">';
		array_filter($data['option'], function($items, $key) use(&$text, $attribute, $value, $inline){
			$checked = in_array($key, $value) ? ' checked="checked"' :'' ;
			$text .= '<div class="checkbox'.$inline.'">';
				$text .= '<label>';
					$text .= '<input type="checkbox"'.$attribute.' value="'.$key.'" '.$checked.'>';
					$text .= ucfirst(trim($items));
				$text .= '</label>';
			$text .= '</div>';
		}, ARRAY_FILTER_USE_BOTH);
		$text .= '</div>';
		$text .= array_key_exists('suffix', $data) ? '<span class="input-group-addon strong">'.$data['suffix'].'</span>' : '';
		unset($data, $attribute);
		return $text;
	}
/*
* TEXT
* MEMBUAT TEXTAREA
* param array $data => [name => string [, placeholder => string [,value => string [,class => string [,prefix => string [,suffix => string]  ]] ] ] ]
*/
	function textarea($data){
		$text = '';
		$text 	.= array_key_exists('prefix', $data) ? '<span class="input-group-addon strong">'.$data['prefix'].'</span>' : '';
		$suffix  = array_key_exists('suffix', $data) ? '<span class="input-group-addon strong">'.$data['suffix'].'</span>' : '';

		$data['class'] 	= array_key_exists('class', $data) ? $data['class'].' form-control' : 'form-control';


		if(array_key_exists('readonly', $data) && $data['readonly'] == true){
			$data['readonly'] = 'readonly';
		}
		else{
			unset($data['readonly']);
		}

		$data['value'] = !empty($this->sess['cache']) && array_key_exists('name', $data) ? $this->sess['cache'][$data['name']] : (array_key_exists('value', $data) ? $data['value'] : '');

		$text .= form_textarea($data);		
		$text .= $suffix;
		$prefix = $suffix = $data =  '';
		return $text;
	}
/*
* TEXT
* MEMBUAT INPUT TYPE FILE
* param array $data => [name => string [,attribut => array ] 
*/
	function file($data){
		$text = '';
			$text .= '<span class="input-group-btn">';
			$text .= '<input type="text" class="form-control image-preview-filename" disabled>';
				$text .= '<button type="button" class="btn btn-default image-preview-clear" style="display:none;">';
                $text .= '</button>';
                $text .= '<div class="btn btn-default uploads-preview-input">';
                        $text .= '<span class="fa fa-folder-open"></span>';
                        $text .= '<span class="image-preview-input-title">Browse</span>';
                        if(array_key_exists('value', $data)){
                        	unset($data['value']);
                        }
                        $text .= form_upload($data);
                $text .= '</div>';
			$text .= '</span>';
		return $text;
	}
/*
* TEXT
* MEMBUAT INPUT TYPE FILE WITH IMAGE SNIPPET
* param array $data => [name => string [,attribut => array ] 
*/
	function filesimage($data){
		$text = '';
			$text .= '<span class="input-group-btn">';
			$text .= '<input type="text" class="form-control image-preview-filename" disabled>';
				$text .= '<button type="button" class="btn btn-default image-preview-clear" style="display:none;">';
                $text .= '</button>';
                $text .= '<div class="btn btn-default uploads-preview-input">';
                        $text .= '<span class="fa fa-folder-open"></span>';
                        $text .= '<span class="image-preview-input-title">Browse</span>';
                        if(array_key_exists('value', $data)){
                        	unset($data['value']);
                        }
                        $text .= form_upload($data);
                $text .= '</div>';
			$text .= '</span>';
		return $text;
	}
/*
* TEXT
* MEMBUAT KELOMPOK IMAGE DENGAN INPUT CHECKBOX
* param array $data => [name => string, option => array [,attribut => array ] 
*/
	function image_checkbox($data){
		$text = '';

		$data['class'] 	= array_key_exists('class', $data) ? $data['class'].' radio-control' : 'radio-control';

		if(!empty($this->sess['cache']) && array_key_exists(preg_replace('/(\[.*\])/', '', $data['name']), $this->sess['cache']) == true){
			$value = (array)$this->sess['cache'][preg_replace('/(\[.*\])/', '', $data['name'])];
		}
		else{
			$value = array_key_exists('value', $data) && empty($this->sess['cache']) ? (array)$data['value'] : array();
		}

		$inline = array_key_exists('inline', $data) && $data['inline'] == true ? ' checkbox-inline' : '';

		$attribute = '';
		unset($data['value']);
		array_walk($data, function($items, $key) use(&$attribute){
			if($key != 'option' && $key != 'inline'){
				$attribute .= ' '.$key.'="'.$items.'"';	
			}
		});		
			$text .= '<ul class="list-group checkbox-group">';
				array_filter($data['option'], function($items, $key) use(&$text, $attribute, $value, $inline){
					$checked = in_array($key, $value) ? '' :' checked="checked"' ;
					$imgcheck = !empty($checked) ? ' img-check' : '';
					$text .= '<li class="list-group-item'.$imgcheck.'">';
						$text .= '<input type="checkbox"'.$attribute.' value="'.$key.'" '.$checked.'>';
						$text .= '<img src="'.base_url($items).'" class="img-responsive">';
					$text .= '</li>';
				}, ARRAY_FILTER_USE_BOTH);
			$text .= '</ul>';
		return $text;		
	}

	function pre_loaded_image($src = null, $array = null){
		$sb = '';
		$sb .= '<br/><div class="row">';
		$sb .= '<div class="col-xs-12">';
				$class = array_key_exists('class', $array) == true ? $array['class'] : '';
				$name = str_replace(array('[',']'), '', $array['name']);
				$sb .= '<div id="'.$name.'-image-result" class="image-result row">';
			if(!empty($src)){
				if(is_array($src)){
					array_walk($src, function($items) use(&$sb, $class){
						$sb .= '<div class="col-xs-12 col-sm-4 col-md-4">';
							$sb .= '<div style="background-image:url('.base_url($items).');" class="image-live-preview '.$class.'"></div>';
						$sb .= '</div>';
					});
				}
				else{
					$sb .= '<div class="col-xs-12 col-sm-4 col-md-4">';
			        	$sb .= '<div style="background-image:url('.base_url($src).');" class="image-live-preview '.$class.'"></div>';		
					$sb .= '</div>';
				}
			}
				$sb .= '</div>';				
		$sb .= '</div>';
		$sb .= '</div>';
		return $sb;	
	}

	function custom($data){
		return $data;
	}

}