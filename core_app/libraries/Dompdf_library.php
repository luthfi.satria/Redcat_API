<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* DISCLAIMER
* LIBRARY DOMPDF LIBRARY FOR EXECUTE HTML AS PDF
* COPYRIGHT : 2016 
* ORIGINAL AUTHOR : LUTHFI SATRIA RAMDHANI
* 1. DILARANG UNTUK MENGAMBIL ALIH HAK CIPTA PEMBUATAN TANPA PERSETUJUAN AUTHOR
* 2. DILARANG MENGHAPUS BARIS DISCLAIMER INI
* 3. DILARANG MENGKOMERSILKAN LIBRARY INI DALAM BENTUK APAPUN KEPADA SIAPAPUN TANPA PERSETUJUAN ATAU TANPA MEMBERIKAN PENGHARGAAN ATAU KONTRIBUSI ATAU DONASI KEPADA SAYA SEBAGAI PENULIS
* 4. ANDA DIPERSILAHKAN MENGUBAH ATAU MENAMBAHKAN ATAU MENYISIPKAN KODE DI DALAM LIBRARY INI DAN MENAMBAHKAN NAMA ANDA SEBAGAI AUTHOR TANPA MENGHILANGKAN NAMA AUTHOR SEBELUMNYA
* 5. ANDA PASTI TAHU DAN MERASAKAN SUSAH PAYAHNYA MEMBUAT SEBUAH KODE PROGRAM, JADI HARGAILAH SESAMA PROGRAMMER
* 6. SEGALA BENTUK KECURANGAN INSYAALLAH AKAN MENDAPATKAN BALASAN DI DUNIA MAUPUN AKHIRAT, TETAPLAH JUJUR KAWAN
*
* TERIMA KASIH
*
*
* SEGALA PERTANYAAN DAN DISKUSI DAPAT ANDA KIRIMKAN KE ALAMAT EMAIL DI BAWAH INI :
* luthfi_its@yahoo.com
*/
use Dompdf\Dompdf;
class Dompdf_library
{
	private $dompdf;
	private $config;

	function __construct(){
		require_once APPPATH.'third_party/dompdf/autoload.inc.php';
		$this->dompdf = new DOMPDF();
		$this->dompdf->set_option('defaultFont', 'Courier');		
	}

	function set_filename($filename = ''){
		$this->config['name'] = $filename;
		return $this;
	}

	function set_papersize($papersize = 'A4', $orientation = 'portrait'){
		$this->dompdf->set_paper($papersize, $orientation);
		return $this;
	}

	function render($contents, $attachment = false){
	    $this->dompdf->load_html($contents);
	    $this->dompdf->render();
	    $this->dompdf->stream($this->config['name'].".pdf", array("Attachment" => $attachment));		
	}

	function save_files($contents){
		$this->dompdf->load_html($contents);
		$this->dompdf->render();
		$output = $this->dompdf->output();
		$path = FCPATH."assets/pdfReports/".$this->config['name'].".pdf";
		file_put_contents($path, $output);
	}
}