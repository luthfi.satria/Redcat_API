<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * AWS SIMPLE STORAGE SERVICES (S3)
 * INSTALLATION
 * 1. get S3 CLIENT Using composer
 *      a. Open Terminal
 *      b. Type : composer require aws/aws-sdk-php
 *      c. Type : composer update
 * 2. Copy this file into your library folder
 * 3. Load this library file from your controller
 */

use Aws\S3\S3Client;
use Aws\Exception\AwsException;

class Awslib{

    var $s3client;
    var $bucketName;

    function credentials($region, $publicKey, $secretKey){
        $credentials = new Aws\Credentials\Credentials($publicKey, $secretKey);

        $this->s3client = new S3Client([
            'version'     => 'latest',
            'region'      => $region,
            'credentials' => $credentials        
        ]);
        return $this;
    }

    function bucketName($bucketName){
        $this->bucketName = $bucketName;
        return $this;
    }

    function create_bucket(){
        $this->s3client->createBucket(['Bucket' => $this->bucketName]);
        return $this;
    }

    function delete_bucket(){
		$this->s3client->deleteBucket(['Bucket' => $this->bucketName]);
        return $this;
    }

    function list_buckets(){
        return $this->s3client->listBuckets();
    }

    function list_objects(){
        return $this->s3client->listObjects(['Bucket' => $this->bucketName]);
    }
/**
 * AWS PUT OBJECT
 * PURPOSE : CREATE AN OBJECT INTO S3 STORAGE
 * PARAM :
 * [	
 *      'Bucket' => 'bucket-name',
 *      'Key' => 'sample/test_image.png',
 *      'Body' => file_get_contents($copying_dir.'original/test_image.png', true)
 * ]
 */
    function put_object($object){
        $param['Bucket'] = $this->bucketName;
        $param['ACL'] = 'public-read';
        if(!empty($object)){
            $param = array_merge($param, $object);
        }
        $this->s3client->putObject($param);
        return $this;        
    }
/**
 * AWS GET OBJECT
 * PURPOSE : GET AN OBJECT FROM S3 STORAGE
 * PARAM : ['Bucket' => 'bucket-name', 'Key' => 'filename / filepath']
 */
    function get_object($key){
        return $this->s3client->getObject([
            'Bucket' => $this->bucketName,
            'Key' => $key
        ]);
    }
/**
 * AWS DELETE OBJECT
 * PURPOSE : DELETE AN OBJECT FROM S3 STORAGE
 * PARAM : ['Bucket' => 'bucket-name', 'Key' => 'filename / filepath']
 */
    function delete_object($key){
        $this->s3client->deleteObject([
            'Bucket' => $this->bucketName,
            'Key' => $key
        ]);
        return $this;        
    }
/**
 * AWS RETRIEVING RESULT
 * PURPOSE : CONVERT S3 RESPONSE INTO ARRAY
 * PARAM : -
 */
    function toArray(){
        return $this->s3client->toArray();
    }
}